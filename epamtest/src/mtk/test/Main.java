package mtk.test;

import java.util.*;

public class Main {

    final static int PEOPLE_IN_BLOCK = 4;
    final static int PEOPLE_IN_TEAM = 7;

    static class ArgumentException extends Exception{
        ArgumentException(){
            super("The number of employees in the block is " + PEOPLE_IN_BLOCK +
                    ". The number of employees by team is " + PEOPLE_IN_TEAM +
                    ". Put please the number of teams by project and the number of blocks per floor. " +
                    "The arguments must be like this: -p2,3,6,8,5 –c16");
        }
    }

    public static void main(final String[] args) {
        try {
            if (args.length != 2) {
                throw new ArgumentException();
            }

            TreeMap<Integer, Integer> teamsInProjectMap = new TreeMap<>();
            Integer blocksInFloor = null;
            int incomingPeople = 0;
            for(String arg: args){
                if (arg.length() < 3)
                    throw new ArgumentException();
                final String a = arg.substring(2);
                if (arg.startsWith("-p")){
                    String[] arr = a.split(",");
                    for(int number = 0; number < arr.length; number++){
                        int teamsInProject = Integer.parseInt(arr[number].trim());
                        int peopleInProject = teamsInProject * PEOPLE_IN_TEAM;
                        incomingPeople += (teamsInProject * PEOPLE_IN_TEAM);
                        System.out.println("number: " + number + " people: " + peopleInProject);
                        teamsInProjectMap.put(teamsInProject, number);
                    }
                }else if (arg.startsWith("-c")){
                    blocksInFloor = Integer.parseInt(a);
                }
            }
            if ((blocksInFloor == null) || (teamsInProjectMap.size()==0)){
                throw new ArgumentException();
            }
            if (blocksInFloor == 0){
                throw new ArgumentException();
            }
            calculate(teamsInProjectMap, blocksInFloor);
            System.out.println("incomingPeople: " + incomingPeople);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    static void calculate(final TreeMap<Integer, Integer> teamsInProjectMap,
                          final int blocksInFloor){

        Integer[] teamsAmountProjectList = new Integer[teamsInProjectMap.size()];
        teamsInProjectMap.keySet().toArray(teamsAmountProjectList);
        Integer[] projectNumbers = new Integer[teamsInProjectMap.size()];
        teamsInProjectMap.values().toArray(projectNumbers);
        int maxPeopleInFloor = blocksInFloor * PEOPLE_IN_BLOCK;
        int peopleInCurrentFloor = maxPeopleInFloor;
        int floor = 1;
        System.out.print("\nFloor " + floor);
        int free = 0;
        int people = 0;

        for(int i = 0; i < teamsAmountProjectList.length;) {
            int projectTeams = teamsAmountProjectList[i];
            int projectNumber = projectNumbers[i];
            int projectPeople = projectTeams * PEOPLE_IN_TEAM;
            if (projectPeople <= peopleInCurrentFloor) {
                System.out.print(" Project " + projectNumber + " (" + projectTeams + " teams)");
                System.out.println(" number: " + projectNumber + " people: " + projectPeople);
                people += projectPeople;
                i++;
                peopleInCurrentFloor -= projectPeople;
            } else {

                while (projectPeople > peopleInCurrentFloor) {
                    int teamsAmount = peopleInCurrentFloor / PEOPLE_IN_TEAM;

                    System.out.print(" Project " + projectNumber + " (" + teamsAmount + " teams)");

                    int peopleFromProject = teamsAmount * PEOPLE_IN_TEAM;

                    System.out.println(" number: " + projectNumber + " people: " + peopleFromProject);
                    projectPeople -= peopleFromProject;

                    int rest = peopleInCurrentFloor - peopleFromProject;
                    System.out.println(" free places: " + rest);
                    free += rest;

                    floor++;
                    System.out.print("\nFloor " + floor);
                    peopleInCurrentFloor = maxPeopleInFloor;
                }


            }
        }
        System.out.println("\nfree places: " + free);
        System.out.println("people: " + people);
    }
}


