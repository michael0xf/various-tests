package my.cash.impl;

import my.cash.ICashModel;

import java.io.*;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: MKRAFT
 * Date: 12/9/13
 * Time: 10:23 PM
 * To change this template use File | Settings | File Templates.
 */

public class CashModel implements ICashModel, Serializable {
    public final static int CURRENCY_LENGTH = 3;
    public final static int MAX_CURRENCY_VALUE = 5000;
    private final HashMap<String, ICashContainer> currencyCashMap = new HashMap<String, ICashContainer>();
    private String fileName;

    public CashModel(final String fileName){
        this.fileName = fileName;
    }

    protected void commit() throws IOException {
        final FileOutputStream fos = new FileOutputStream(fileName);
        final ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(this);
        oos.flush();
        oos.close();
    }

    public static void validateCurrencyValue(final int value) throws CashException{
        if (value > MAX_CURRENCY_VALUE)
            throw new CashException("Currency value "+ value + ", this more than maximum " + MAX_CURRENCY_VALUE);
        final String stringValue = Integer.toString(value, 10);
        final int firstChar = stringValue.charAt(0);
        if ((firstChar != '1')&&(firstChar != '5'))
            throw new CashException("Invalid currency value" + stringValue);
        for(int i = 1; i < stringValue.length(); i++){
            if (stringValue.charAt(i) != '0')
                throw new CashException("Invalid currency value " + stringValue + " int position " + i);
        }
    }

    public static void validateCurrency(final String currency) throws CashException{
        if ((currency == null)||(currency.length() != CURRENCY_LENGTH)){
            throw new CashException("Invalid currency: " + currency);
        }
        for(int i = 0; i < CURRENCY_LENGTH; i++){
            final int ch = currency.charAt(i);
            if ((ch < 'A') || (ch > 'Z')){
                throw new CashException("Invalid currency " + currency);
            }
        }
    }

    public Collection<ICashContainer> getCashContainers(){
        return currencyCashMap.values();

    }

    public ICashContainer getCashContainer(final String currency) throws CashException{
        validateCurrency(currency);
        ICashContainer cashContainer = currencyCashMap.get(currency);
        if (cashContainer == null){
            cashContainer = new CashContainer(currency.charAt(0), currency.charAt(1), currency.charAt(2));
            currencyCashMap.put(currency, cashContainer);
        }
        return cashContainer;

    }

    private class CashContainer  implements ICashContainer{
        private int firstChar, secondChar, thirdChar;
        private final HashMap<Integer, INoteContainer> valueNotesMap = new HashMap<Integer, INoteContainer>();

        public CashContainer(final int firstChar, final int secondChar, final int thirdChar) {
            this.firstChar = firstChar;
            this.secondChar = secondChar;
            this.thirdChar = thirdChar;
        }

        @Override
        public synchronized void getCash(final int amount) throws CashException, IOException {
            if (getAmount() < amount){
                throw new CashException("Not enough " + getCurrency());
            }
            final Collection<ICashModel.INoteContainer> noteContainers = getNoteContainers();
            int cash = amount;
            for(final ICashModel.INoteContainer noteContainer: noteContainers){
                cash = noteContainer.prepareCash(cash);
            }
            if (cash != 0)
                throw new CashException("Invalid amount " + amount);
            for(final ICashModel.INoteContainer noteContainer: noteContainers){
                noteContainer.getNotes();
            }
        }

        @Override
        public synchronized INoteContainer getNoteContainer(final int value) throws CashException{
            validateCurrencyValue(value);
            INoteContainer notes = valueNotesMap.get(value);
            if (notes == null){
                notes = new NoteContainer(value);
                valueNotesMap.put(value, notes);
            }
            return notes;
        }

        @Override
        public String getCurrency() {
            return "" + firstChar + secondChar + thirdChar;
        }

        @Override
        public synchronized int getAmount() {
            int amount = 0;
            final Set<Integer> set = valueNotesMap.keySet();
            for(final Integer value: set){
                final INoteContainer noteContainer = valueNotesMap.get(value);
                amount += value * noteContainer.getNoteInfo().getNumber();
            }
            return amount;
        }

        @Override
        public Collection<INoteContainer> getNoteContainers() {
            return valueNotesMap.values();
        }

        private class NoteContainer implements INoteContainer, INoteInfo {
            private int value;
            private int number = 0;
            private int prepared = 0;

            private NoteContainer(int value){
                this.value = value;
            }

            @Override
            public INoteInfo getNoteInfo(){
                return this;
            }

            @Override
            public int getValue() {
                return value;
            }

            @Override
            public int getNumber() {
                return number;
            }

            @Override
            public String getCurrency() {
                return CashContainer.this.getCurrency();
            }

            @Override
            public synchronized void addNotes(final int number) throws CashException, IOException {
                long sum = (long)this.number + (long)number;
                if (sum > Integer.MAX_VALUE) {
                    throw new CashException("Overflow container of banknotes.");
                }
                this.number += number;
                commit();
            }

            @Override
            public synchronized void getNotes() throws CashException, IOException {
                this.number = this.number - this.prepared;
                this.prepared = 0;
                commit();
            }

            @Override
            public synchronized int prepareCash(int cash){
                final int value = getValue();
                int dec = cash / value;
                if (dec > 0){
                    int number = getNumber();
                    if (dec > number)
                        dec = number;
                    prepared = dec;
                    cash = cash - (dec * value);
                }
                return cash;
            }

            @Override
            public int compareTo(final INoteInfo o) {
                return this.getValue() - o.getValue();
            }
        }
    }
}
