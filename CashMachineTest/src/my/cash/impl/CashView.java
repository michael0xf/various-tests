package my.cash.impl;

import my.cash.ICashController;
import my.cash.ICashModel;
import my.cash.ICashView;

import java.io.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: MKRAFT
 * Date: 12/9/13
 * Time: 3:56 AM
 * To change this template use File | Settings | File Templates.
 */
public class CashView implements ICashView{

    final static int ADD = '+';
    final static int GET = '-';
    final static int PRINT = '?';
    final static String SPLITTER = " ";
    private PrintStream printStream;
    private InputStream inputStream;

    public CashView(final InputStream inputStream, final PrintStream printStream){
        setInputStream(inputStream);
        setPrintStream(printStream);
    }

    protected InputStream getInputStream() {
        return inputStream;
    }

    protected void setInputStream(final InputStream inputStream) {
        this.inputStream = inputStream;
    }

    protected PrintStream getPrintStream() {
        return printStream;
    }

    protected void setPrintStream(final PrintStream printStream) {
        this.printStream = printStream;
    }

    @Override
    public void readArguments(final ICashController cashController){
        final BufferedReader reader = new
                BufferedReader(new InputStreamReader(getInputStream()));
        String line;
        try {
            while((line = reader.readLine()) != null){
                final String[] args = line.split(SPLITTER);
                pushArguments(cashController, args);
            }
        } catch (IOException e) {
            cashController.handleError(e);
        }
    }

    public void pushArguments(final ICashController cashController, final String[] arguments){
        try{
            if (arguments.length == 0)
                return;
            String arg = arguments[0];
            if (arg.length() != 1){
                new CashException("Unknow command " + arg);
            }
            goToCommand(cashController, arg.charAt(0), arguments);
        }catch (CashException e){
            cashController.handleError(e);
        }catch (NumberFormatException e){
            cashController.handleError(e);
        }catch (IOException e){
            cashController.handleError(e);
        }
    }

    protected void addNotes(final ICashController cashController, final String[] arguments) throws NumberFormatException, CashException, IOException {
        if (arguments.length != 4)
            throw new CashException("Invalid count of arguments, must be + <currency> <value> <number>");
        cashController.addNotes(arguments[1], Integer.parseInt(arguments[2]), Integer.parseInt(arguments[3]));
    }

     protected void getCash(final ICashController cashController, final String[] arguments) throws NumberFormatException, CashException, IOException {
         if (arguments.length != 3)
             cashController.handleError("Invalid count of arguments, must be - <currency> <amount>");
         cashController.getCash(arguments[1], Integer.parseInt(arguments[2]));
     }

     protected void goToCommand(final ICashController cashController, final int command, final String[] arguments) throws NumberFormatException, CashException, IOException {
        switch(command){
            case ADD:
                addNotes(cashController, arguments);
                break;
            case GET:
                getCash(cashController, arguments);
                break;
            case PRINT:
                if (arguments.length != 1)
                    throw new CashException("Invalid count of arguments, must be one.");
                printCash(cashController);
                break;
            default:
                throw new CashException("Unknow command " + command);
        }

    }

    @Override
    public void printCash(final ICashController cashController){
        final List<ICashModel.INoteInfo> list = cashController.getCashInfo();
        for(final ICashModel.INoteInfo noteInfo: list){
            final String currency = noteInfo.getCurrency();
            final int value = noteInfo.getValue();
            final int number = noteInfo.getNumber();
            printMessage(currency + SPLITTER + value + SPLITTER + number);
        }
    }

    @Override
    public void printOk(){
        getPrintStream().println("OK");
    }

    @Override
    public void printError(){
        getPrintStream().println("ERROR");
    }

    @Override
    public void printMessage(final String message){
        getPrintStream().println(message);
    }


}
