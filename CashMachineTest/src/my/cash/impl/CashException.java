package my.cash.impl;

/**
 * Created with IntelliJ IDEA.
 * User: MKRAFT
 * Date: 12/9/13
 * Time: 11:09 PM
 * To change this template use File | Settings | File Templates.
 */
public class CashException extends Exception {
    public CashException(String message) {
        super(message);
    }
}
