package my.cash.impl;

import my.cash.IAtm;
import my.cash.ICashController;
import my.cash.ICashModel;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: MKRAFT
 * Date: 12/9/13
 * Time: 3:55 AM
 * To change this template use File | Settings | File Templates.
 */
public class Machine implements ICashController{

    private CashView cashView;
    private CashModel cashModel;

    public Machine() throws IOException, ClassNotFoundException {
        init();
    }

    public IAtm getAtm(){
        return this;
    }

    protected CashModel loadCash() throws IOException, ClassNotFoundException {
        FileInputStream fis = null;
        CashModel cashModel;
        final String fileName = "cash";
        try {
            fis = new FileInputStream(fileName);
            final ObjectInputStream oin = new ObjectInputStream(fis);
            cashModel = (CashModel) oin.readObject();
            oin.close();
        } catch (FileNotFoundException e) {
            cashModel = new CashModel(fileName);
        }finally {
            if (fis != null)
                fis.close();
        }
        return cashModel;
    }

    protected void init() throws IOException, ClassNotFoundException {
        final CashView cashView = new CashView(System.in, System.out);
        setCashView(cashView);
        setCashModel(loadCash());
    }

    protected void setCashView(CashView cashView) {
        this.cashView = cashView;
    }

    protected void setCashModel(CashModel cashModel) {
        this.cashModel = cashModel;
    }

    protected CashModel getCashModel() {
        return cashModel;
    }


    protected CashView getCashView() {
        return cashView;
    }

    @Override
    public List<ICashModel.INoteInfo> getCashInfo(){
        final List<ICashModel.INoteInfo> info = new ArrayList<ICashModel.INoteInfo>();
        final Collection<ICashModel.ICashContainer> cashContainers = getCashModel().getCashContainers();
        for(final ICashModel.ICashContainer cashContainer: cashContainers){
            final Collection<ICashModel.INoteContainer> noteContainers = cashContainer.getNoteContainers();
            for(final ICashModel.INoteContainer noteContainer: noteContainers){
                info.add(noteContainer.getNoteInfo());
            }
        }
        return info;
    }

    @Override
    public void printCash() throws CashException{
        getCashView().printCash(this);
    }

    @Override
    public void addNotes(final String currency, final int value, final int number) throws CashException, IOException {
        try {
            final ICashModel.ICashContainer cashContainer = getCashModel().getCashContainer(currency);
            final ICashModel.INoteContainer notes = cashContainer.getNoteContainer(value);
            notes.addNotes(number);
        } catch (CashException e) {
            handleError(e);
            throw e;
        } catch (IOException e) {
            handleError(e);
            throw e;
        }
    }

    @Override
    public void getCash(final String currency, final int amount) throws CashException, IOException {
        try {
            final ICashModel.ICashContainer cashContainer = getCashModel().getCashContainer(currency);
            cashContainer.getCash(amount);
        } catch (CashException e) {
            handleError(e);
            throw e;
        } catch (IOException e) {
            handleError(e);
            throw e;
        }
    }

    @Override
    public void handleError(final Exception e){
        getCashView().printError();
        getCashView().printMessage(e.getMessage());
    }

    @Override
    public void handleError(final String message){
        getCashView().printMessage(message);
    }


    public static void main(final String[] args){
        try{
            final Machine machine = new Machine();
            if ((args == null)||(args.length == 0))
                machine.getCashView().readArguments(machine);
            else
                machine.getCashView().pushArguments(machine, args);

        }catch (Throwable throwable){
            throwable.printStackTrace();
        }
    }

}
