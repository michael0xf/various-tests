package my.cash;

import my.cash.impl.CashException;

import java.io.IOException;
import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: MKRAFT
 * Date: 12/9/13
 * Time: 4:06 AM
 * To change this template use File | Settings | File Templates.
 */
public interface ICashModel {
    public interface ICurrency{
        public String getCurrency();
    }
    public interface ICashContainer extends ICurrency{
        public int getAmount();
        public Collection<INoteContainer> getNoteContainers();
        public INoteContainer getNoteContainer(final int value) throws CashException;
        public void getCash(int amount) throws CashException, IOException;
    }
    public interface INoteInfo extends ICurrency, Comparable<INoteInfo>{
        public int getValue();
        public int getNumber();
    }
    public interface INoteContainer extends Comparable<INoteInfo>{
        public INoteInfo getNoteInfo();
        public void addNotes(final int number) throws CashException, IOException;
        public int prepareCash(final int amount) throws CashException;
        public void getNotes() throws CashException, IOException;
    }
    public Collection<ICashContainer> getCashContainers();
    public ICashContainer getCashContainer(final String currency) throws CashException;
}
