package my.cash;

import my.cash.impl.CashException;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: MKRAFT
 * Date: 12/9/13
 * Time: 4:16 PM
 * To change this template use File | Settings | File Templates.
 */
public interface IAtm {
    public void addNotes(String currency, int value, int number) throws CashException, IOException;
    public void getCash(String currency, int amount) throws CashException, IOException;
    public void printCash() throws CashException;
}
