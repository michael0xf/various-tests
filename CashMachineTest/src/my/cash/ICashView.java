package my.cash;

/**
 * Created with IntelliJ IDEA.
 * User: MKRAFT
 * Date: 12/9/13
 * Time: 4:06 AM
 * To change this template use File | Settings | File Templates.
 */
public interface ICashView {
    public void readArguments(ICashController cashController);
    public void printMessage(String message);
    public void printOk();
    public void printError();
    public void printCash(ICashController cashController);
}
