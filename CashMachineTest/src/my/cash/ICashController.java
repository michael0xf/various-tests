package my.cash;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: MKRAFT
 * Date: 12/9/13
 * Time: 4:06 AM
 * To change this template use File | Settings | File Templates.
 */
public interface ICashController extends IAtm{
    public List<ICashModel.INoteInfo> getCashInfo();
    public void handleError(Exception exception);
    public void handleError(String message);
}
