package mtk.test.csssr;
import java.util.Arrays;
import java.util.Comparator;
/** Есть строка, состоящая из слов. Все слова в ней разделены одним пробелом.
 Нужно преобразовать строку в такую структуру данных, которая группирует
 слова по первой букве в слове. Затем вывести только группы, содержащие
 более одного элемента.
 Группы должны быть отсортированы в алфавитном порядке. Слова внутри группы
 нужно сортировать по убыванию количества символов; если количество символов
 равное, то сортировать в алфавитном порядке.
 Пример строки: String s = «сапог сарай арбуз болт бокс биржа»
 Отсортированная строка: [б=[биржа, бокс, болт], c=[сапог, сарай]]
 * @author <a href="mailto:mtkravchenko@gmail.com">Mikhail Kravchenko</a>
 * @version 1.0
 */
public class TestTask{


    /** Get result String
     * @param words some words separated by a single space.
     * @return A result string according sort rule, "[]" in case of empty result
     */
    public String getResultString(final String words) throws Exception{
        if ((long)Integer.MAX_VALUE < (long)Character.MAX_VALUE << 2){
            throw new Exception("This VM not supported");
        }
        if (isEmpty(words)){
            return "[]";
        }
        final String[] args =  words.split(" ");
        if (isEmpty(args)){
            return "[]";
        }
        Arrays.sort(args, new StringComparator());
        return formatResults(args);
    }

    @org.junit.Test
    public void test() {
        try{
            String[][] testData = getTestData();
            for(int i = 0; i < testData.length; i++) {
                String[] data = testData[i];
                assert (data.length == 2);
                final String result = getResultString(data[0]);
                final String expected = data[1];

                if (!result.equals(expected)){
                    System.out.println("error at line " + i + ": " + data[0]);
                    System.out.println("result: " + result);
                    System.out.println("expected: "  + expected);
                    for(int c = 0; c < result.length() && c < expected.length(); c++){
                        final char c0 = result.charAt(c);
                        final char c1 = expected.charAt(c);
                        if (c0 != c1){
                            System.out.println("char '" + c0 + "' != '" + c1 + "' at " + c + " from " + result.substring(c));
                            break;
                        }
                    }
                    assert(false);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            assert false;
        }
    }

    protected String[][] getTestData(){
        return new String[][]{
                {"сапог сарай арбуз болт бокс биржа", "[б=[биржа, бокс, болт], с=[сапог, сарай]]"},
                {"сапог сарай Барбуз болт бокс Биржа", "[Б=[Барбуз, Биржа], б=[бокс, болт], с=[сапог, сарай]]"},
                {"сапог сарай Сок Барбуз болт Сорока бокс Биржа", "[Б=[Барбуз, Биржа], б=[бокс, болт], С=[Сорока, Сок], с=[сапог, сарай]]"},
                {"зАмок замашка замОк замах", "[з=[замашка, зАмок, замах, замОк]]"},
                {"сапог Сарай арбуз болт бокс биржа", "[б=[биржа, бокс, болт]]"},
                {"сапог арбуз болт сарай бокс биржа", "[б=[биржа, бокс, болт], с=[сапог, сарай]]"},
                {"сапог #болт #Болт сарай арбуз #бокс #Биржа", "[с=[сапог, сарай], #=[#Биржа, #Болт, #бокс, #болт]]"},
                {"сапог ,болт ,Болт сарай арбуз ,бокс ,Биржа", "[с=[сапог, сарай], ,=[,Биржа, ,Болт, ,бокс, ,болт]]"},
                {"сапог Сарай арбуз болт бокс иржа", "[б=[бокс, болт]]"}};
    };

    protected static boolean isEmpty(final String arg){
        return arg == null || arg.length() == 0;
    }

    protected static boolean isEmpty(final String[] args){
        return (args == null) || (args.length == 0);
    }

    protected String formatResults(final String[] args){
        final StringBuilder result = new StringBuilder();
        final StringBuilder wordForLetter = new StringBuilder();
        int firstChar = -1;
        int wordCounter = 0;
        for (final String word : args) {
            if (isEmpty(word)) {
                continue;
            }
            int c0 = word.charAt(0);
            if (c0 != firstChar) {
                if (wordCounter > 1){
                    addWords(result, firstChar, wordForLetter);
                }
                wordForLetter.delete(0, wordForLetter.length());
                wordCounter = 0;
                firstChar = c0;
            }
            if (wordForLetter.length() > 0){
                wordForLetter.append(", ");
            }
            wordCounter++;
            wordForLetter.append(word);
        }
        if (wordCounter > 1) {
            addWords(result, firstChar, wordForLetter);
        }
        result.insert(0, '[');
        result.append(']');
        return result.toString();
    }

    protected void addWords(StringBuilder result, int firstChar, StringBuilder wordForLetter){
        if (result.length() > 0){
            result.append(", ");
        }
        result.append((char)firstChar + "=[");
        result.append(wordForLetter);
        result.append(']');
    }

    class StringComparator implements Comparator<String>{
        @Override
        public int compare(final String o1, final String o2) {
            if (o1 == o2) {
                return 0;
            }
            if (isEmpty(o1)){
                return 1;
            }
            if (isEmpty(o2)){
                return -1;
            }
            if (o1.equals(o2)) {
                return 0;
            }
            return stringCompare(o1, o2);
        }
        protected int stringCompare(final String str1, final String str2){
            int c1 = str1.charAt(0);
            int c2 = str2.charAt(0);
            c1 = letterCodeToAaBbCc(c1);
            c2 = letterCodeToAaBbCc(c2);
            if ( c1 != c2 ){
                return c1 - c2;
            }
            if (str1.length() != str2.length()) {
                return str2.length() - str1.length();
            }
            return internalCompare(str1, str2);
        }

        protected int letterCodeToAaBbCc(final int c) {
            if (Character.isLetter(c)){
                if(Character.isLowerCase(c)){
                    return (Character.toUpperCase(c) << 1) + 1;
                }else{
                    return (c << 1);
                }
            }else{
                return Character.MAX_VALUE * 3 + c;
            }
        }

        protected int internalCompare(final String str1, final String str2) {
            for (int i = 0; i < str1.length() &&
                    i < str2.length(); i++) {
                int c1 = str1.charAt(i);
                int c2 = str2.charAt(i);
                if (c1 == c2) {
                    continue;
                } else {
                    c1 = letterCodeToAaBbCc(c1);
                    c2 = letterCodeToAaBbCc(c2);
                    return c1 - c2;
                }
            }
            return 0;
        }
    }


}
