package midnight.mtk;

import javax.swing.text.BadLocationException;
import javax.swing.text.EditorKit;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import java.io.*;
import java.net.*;
import java.text.Collator;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;


/**
 * @author <a href="mailto:mtkravchenko@gmail.com">Mikhail Kravchenko</a>
 */
public class WWWWordCounter implements Const {

    protected String getDefaultEncoding() {
        return "utf-8";
    }


    /**
     * Counting words in "http://..."
     *
     * @param address http://...
     * @param stream  output stream.
     * @throws midnight.mtk.WWWWordCounter.HomeException
     *          with message
     */
    public void countWords(final String address, final PrintStream stream) throws HomeException {
        final URL url = getURL(address);
        stream.println("Counting words in " + url.toString() + "...");
        final URLConnection urlConnection = openConnection(url);
        final String encoding = getEncoding(urlConnection);
        final char[] data = getData(urlConnection, encoding);
        final HashMap<String, Integer> words = countWords(data);
        final String[] sortWords = sortWords(words, encoding);
        printResult(words, sortWords, stream);
    }

    /**
     * Generate HomeException with message
     *
     * @param url http://...
     * @throws midnight.mtk.WWWWordCounter.HomeException
     *          generated exception
     */

    protected void errorNoHtml(final URL url) throws HomeException {
        throw new HomeException("HTML-data not found by this address: " + url.toString());
    }


    /**
     * Generate HomeException with message
     *
     * @param url http://...
     * @throws midnight.mtk.WWWWordCounter.HomeException
     *          generated exception
     */

    protected void errorIncorrectHtml(final URL url) throws HomeException {
        throw new HomeException("Incorrect HTML was found by this address: " + url.toString());
    }

    /**
     * Verify and get URL
     *
     * @param arg http://...
     * @return calculated URL
     * @throws midnight.mtk.WWWWordCounter.HomeException
     *          with message
     */

    protected URL getURL(String arg) throws HomeException {
        arg = arg.trim();
        if (!arg.toLowerCase().startsWith("http://")) {
            throw new HomeException("\"" + arg + "\" don't starts with 'HTTP://'.");
        }
        URI uri;
        try {
            uri = new URI(arg);
        } catch (URISyntaxException e) {
            throw new HomeException("Syntax error: " + e.getMessage());
        }
        URL url;
        try {
            url = uri.toURL();
        } catch (MalformedURLException e) {
            throw new HomeException("Incorrect URL: " + e.getMessage());
        }
        return url;
    }

    /**
     * Open connection by URL
     *
     * @param url http://...
     * @return new connection
     * @throws midnight.mtk.WWWWordCounter.HomeException
     *          with message
     */

    protected URLConnection openConnection(URL url) throws HomeException {
        try {
            URLConnection urlConnection = url.openConnection();
            try {
                urlConnection.connect();
                return urlConnection;
            } catch (IOException e) {
                throw new HomeException("Connecting error: " + e.getMessage());
            }
        } catch (IOException e) {
            throw new HomeException("IO error by opening connection: " + e.getMessage());
        }
    }


    /**
     * Get charset
     *
     * @param urlConnection opened connection
     * @return encoding
     * @throws midnight.mtk.WWWWordCounter.HomeException
     *          with message
     */

    protected String getEncoding(URLConnection urlConnection) throws HomeException {
        String encoding = null;
        String contentType = urlConnection.getContentType();
        if (contentType != null) {
            contentType = contentType.toLowerCase();
            if (contentType.indexOf("html") == ERROR) {
                errorNoHtml(urlConnection.getURL());
            }
            int charsetIndex = contentType.indexOf("charset=");
            if (charsetIndex > ERROR) {
                String s = contentType.substring(charsetIndex + "charset=".length());
                String[] res = s.split(";");
                encoding = res[0].trim();
            }
        }

        if (encoding == null) {
            encoding = urlConnection.getContentEncoding();
            if (encoding == null) {
                encoding = getDefaultEncoding();
            }
        }
        return encoding;
    }

    /**
     * Get text data from html document, ignore internal charset directives
     *
     * @param urlConnection opened connection
     * @param encoding      charset
     * @return whole text data without breaks
     * @throws midnight.mtk.WWWWordCounter.HomeException
     *          with message
     */

    protected char[] getData(final URLConnection urlConnection, final String encoding) throws HomeException {
        InputStream is = null;
        try {
            is = urlConnection.getInputStream();
            final EditorKit kit = new HTMLEditorKit();
            final HTMLDocument doc = (HTMLDocument) kit.createDefaultDocument();
            doc.putProperty("IgnoreCharsetDirective", true);
            final BufferedReader dataInputStream = new BufferedReader(new InputStreamReader(is, encoding));
            kit.read(dataInputStream, doc, 0);
            return doc.getText(0, doc.getLength()).toCharArray();
        } catch (IOException e) {
            close(is);
            throw new HomeException("Error while receiving data: " + e.getMessage());
        } catch (BadLocationException e) {
            close(is);
            errorIncorrectHtml(urlConnection.getURL());
        } finally {
            close(is);
        }
        return null;
    }


    /**
     * Close resource as tradition, not necessary
     *
     * @param is opened stream
     */

    protected void close(InputStream is) {
        try {
            if (is != null) {
                is.close();
            }
        } catch (Throwable e) {
        }
    }

    /**
     * Counting words which in the array
     *
     * @param data chars
     * @return map of words with numbers of occurrences
     */
    protected HashMap<String, Integer> countWords(final char[] data) {
        final HashMap<String, Integer> words = new HashMap();
        int beginWord = 0;
        for (int i = 0; i < data.length; i++) {
            final char c = data[i];
            if (!Character.isLetter(c)) {
                if (beginWord > ERROR) {
                    if (isFixWordCharacter(c)) {
                        putWord(words, data, beginWord, i);
                    }
                    beginWord = ERROR;
                }
            } else {
                if (beginWord == ERROR) {
                    beginWord = i;
                }
            }
        }
        return words;
    }

    /**
     * Check delimiter
     *
     * @param c symbol
     * @return if delimiter
     */
    protected boolean isFixWordCharacter(int c) {
        switch (c) {
            case ' ':
            case ',':
            case '-':
            case '.':
            case ';':
            case '\n':
            case '\r':
                return true;
            default:
                return false;
        }
    }

    /**
     * Create new word and put it in the table
     *
     * @param words     table with numbers of occurrences
     * @param data      chars
     * @param beginWord in chars
     * @param end       of word in chars
     */

    protected void putWord(final HashMap<String, Integer> words, final char[] data, final int beginWord, final int end) {
        int len = end - beginWord;
        if (len > 0) {
            String s = new String(data, beginWord, len);
            if (words.containsKey(s)) {
                int counter = words.get(s);
                counter++;
                words.put(s, counter);
            } else
                words.put(s, 1);

        }
    }

    /**
     * Sorting of all word from the table
     *
     * @param words    table with numbers of occurrences
     * @param encoding charset for sorting
     * @return sorted words
     */

    protected String[] sortWords(final HashMap<String, Integer> words, final String encoding) {
        final String[] sortWords = new String[words.size()];
        words.keySet().toArray(sortWords);
        Arrays.sort(sortWords, 0, sortWords.length, Collator.getInstance(new Locale(encoding)));
        return sortWords;
    }

    /**
     * Print results to stream
     *
     * @param words     table with numbers of occurrences
     * @param sortWords ordered words
     * @param stream    output stream
     */

    protected void printResult(final HashMap<String, Integer> words, final String[] sortWords, final PrintStream stream) {
        for (String w : sortWords) {
            stream.println(words.get(w) + " " + w);
        }
        stream.println("Total: " + sortWords.length);
    }

    public static class HomeException extends Exception {
        HomeException(String message) {
            super(message);
        }
    }
}
