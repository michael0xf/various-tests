package midnight.mtk;

/**
 * @author <a href="mailto:mtkravchenko@gmail.com">Mikhail Kravchenko</a>
 */
public class Tests {

    static WWWWordCounter wwwWordCounter = new WWWWordCounter();

    public static void main(final String... args) {

        if ((args != null) && (args.length > 0)) {
            for (String arg : args) {
                test(arg);
            }
        } else {
            test("bred");
            test("https://gmail.com");
            test("http://lib.ru");
            test("http://google.com");
            test("http://www.bbc.com/russian/news");
        }
    }

    static void test(final String arg) {
        try {
            wwwWordCounter.countWords(arg, System.out);
            System.out.println("-------------------------------------");
        } catch (WWWWordCounter.HomeException e) {
            System.out.println(e.getMessage());
        }
    }

}
