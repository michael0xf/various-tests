package midnight.mtk;


/**
 * @author <a href="mailto:mtkravchenko@gmail.com">Mikhail Kravchenko</a>
 */
public class Test implements Const {

    public static void main(final String... args) {
        try {
            if ((args == null) || (args.length != 1) || (args[0] == null)) {
                System.out.println("Argument must be as 'http://...'");
                System.exit(ERROR);
            }
            new WWWWordCounter().countWords(args[0], System.out);
        } catch (WWWWordCounter.HomeException e) {
            System.out.println(e.getMessage());
            System.exit(ERROR);
        } catch (Throwable e) {
            e.printStackTrace();
            System.exit(ERROR);
        }
    }

}
