package mtk.xmlutils;

/**
 * Created with IntelliJ IDEA. User: michaelk Date: 11/11/13 Time: 8:00 AM To
 * change this template use File | Settings | File Templates.
 */
public interface XMLNames {
    // Types
    String LLVAR = "LLVAR";
    String IFSF = "IFSF";

    // typical XML names
    String NEVER = "N";
    String ECHOES = "Echoes";
    String ECHO = "Echo";
    String REQUEST_ID = "RequestId";
    String RESPONSE_ID = "ResponseId";
    String PROCESSES = "Processes";
    String PROCESS = "Process";
    String VALUE = "Value";
    String PRESENCES = "Presences";
    String MAX_LENGTH = "MaxLength";
    String ID = "Id";
    String TYPE = "Type";
    String FIELDS = "Fields";
    String FIELD = "Field";
    String NAME = "Name";
    String DATA_ELEMENT_NAME = "DataName";
    String FORMAT = "Format";
    String PRESENCE = "Presence";

    // FIRST DOC:
    String MESSAGES = "Messages";
    String MESSAGE = "Message";
    String SUBFIELD = "SubField";
    String SUBFIELDS = "SubFields";
    String TAGS = "Tags";
    String TAG = "Tag";

}
