package mtk.xmlutils;

import mtk.xmlcash.EquensFileNames;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: michaelk
 * Date: 11/11/13
 * Time: 8:00 AM
 * To change this template use File | Settings | File Templates.
 */
//This utility is not used directly in simulator. It is used to create XML.
public class FieldsXmlCreator {


    void fieldsXmlCreate(final String srcFile, final String dstFile) throws ParserConfigurationException, SAXException, IOException, XMLException, TransformerException {
        final File fXmlFile = new File(srcFile);
        final DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        final DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        final Document doc = dBuilder.parse(fXmlFile);
        doc.getDocumentElement().normalize();
        Log.out("Root element :" + doc.getDocumentElement().getNodeName());
        final HashMap<String, HashMap<String, Element>> fieldsMap = new HashMap<String, HashMap<String, Element>>();  //HashMap<FieldNumber, HashMap<MessageId, Field>>
        readMessages(fieldsMap, doc.getElementsByTagName(XMLNames.MESSAGE));
        writeFieldsDocument(fieldsMap, doc.getXmlEncoding(), doc.getXmlVersion(), dstFile);
    }

    void readMessages(final HashMap<String, HashMap<String, Element>> fieldsMap, final NodeList nList) throws XMLException {
        for (int temp = 0; temp < nList.getLength(); temp++) {
            final Node nNode = nList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                final Element eElement = (Element) nNode;
                final String messageId = eElement.getAttribute(XMLNames.ID);
                readFields(fieldsMap, eElement.getElementsByTagName(XMLNames.FIELDS), messageId);
            }
        }
    }

    void readFields(final HashMap<String, HashMap<String, Element>> fieldsMap, final NodeList nList, String messageId) throws XMLException {
        if (nList.getLength() != 1)
            throw new XMLException("invalid count of bodies in MessageBody " + messageId);
        final Node nNode = nList.item(0);
        if (nNode.getNodeType() == Node.ELEMENT_NODE) {
            final Element eElement = (Element) nNode;
            readElement(fieldsMap, eElement.getElementsByTagName(XMLNames.FIELD), messageId);
        }
    }

    void readElement(final HashMap<String, HashMap<String, Element>> fieldsMap, final NodeList nList, String messageId) {
        if (nList == null)
            return;
        for (int temp = 0; temp < nList.getLength(); temp++) {
            final Node nNode = nList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                final Element eElement = (Element) nNode;
                putField(fieldsMap, eElement, messageId);
            }
        }
    }

    void putField(final HashMap<String, HashMap<String, Element>> fieldsMap, Element field, String messageId) {
        final String elementNumber = field.getAttribute(XMLNames.NAME);
        HashMap<String, Element> presents = fieldsMap.get(elementNumber);
        if (presents == null) {
            presents = new HashMap<String, Element>();
            fieldsMap.put(elementNumber, presents);
        }
        presents.put(messageId, field);
    }

    void writeFieldsDocument(final HashMap<String, HashMap<String, Element>> fieldsMap, final String encoding, final String xmlVersion, final String dstFile) throws TransformerException, ParserConfigurationException, IOException {
        final Document document = newDocument();
        document.setXmlVersion(xmlVersion);
        final Element fieldsTag = document.createElement(XMLNames.FIELDS);
        writeFields(document, fieldsMap, fieldsTag);
        document.appendChild(fieldsTag);
        final String xml = getCurrentXml(document, encoding);
        writeFile(dstFile, xml, encoding);
    }

    Document newDocument() throws ParserConfigurationException {
        final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        final DocumentBuilder docBuilder = factory.newDocumentBuilder();
        final Document document = docBuilder.newDocument();
        final Comment comment = document.createComment("This file is created automatically.");
        document.appendChild(comment);
        return document;
    }

    void writeFile(final String fileName, final String text, final String encoding) throws IOException {
        final FileOutputStream fileOutputStream = new FileOutputStream(fileName);
        final BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(fileOutputStream, encoding));
        bufferedWriter.append(text);
        bufferedWriter.flush();
        bufferedWriter.close();
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    void writeFields(final Document document, final HashMap<String, HashMap<String, Element>> fieldsMap, final Node newNode) {
        final SortedSet<String> keys = createNumberNumberComparator(fieldsMap.keySet());
        for (final String key : keys) {
            final HashMap<String, Element> elements = fieldsMap.get(key);
            final Element newElement = document.createElement(XMLNames.FIELD);
            final Element firstElement = elements.values().iterator().next();
            writeFieldNumberFormat(firstElement, newElement);
            newNode.appendChild(newElement);
            writeMessageIds(document, newElement, elements);
            if (firstElement.getElementsByTagName(XMLNames.SUBFIELDS).getLength() > 0) //if field contains subFields
                readAndWriteElements(document, newElement, elements, XMLNames.SUBFIELDS, XMLNames.SUBFIELD);
            else if (firstElement.getElementsByTagName(XMLNames.TAGS).getLength() > 0) //if field contains subFields
                readAndWriteElements(document, newElement, elements, XMLNames.TAGS, XMLNames.TAG);

        }
    }

    void writeFieldNumberFormat(final Element oldField, final Element newField) {
        newField.setAttribute(XMLNames.NAME, oldField.getAttribute(XMLNames.NAME));
        final String format = oldField.getAttribute(XMLNames.FORMAT);
        if (format.length() > 0)
            newField.setAttribute(XMLNames.FORMAT, format);
        final String maxLength = oldField.getAttribute(XMLNames.MAX_LENGTH);
        if (maxLength.length() > 0)
            newField.setAttribute(XMLNames.MAX_LENGTH, oldField.getAttribute(XMLNames.MAX_LENGTH));
    }

    SortedSet<String> createNumberNumberComparator(Set<String> set) {
        final Comparator<String> comparator = new Comparator<String>() {
            public int compare(String o1, String o2) {
                Integer i1 = Integer.parseInt(o1, 16);
                Integer i2 = Integer.parseInt(o2, 16);
                return i1.compareTo(i2);
            }
        };
        final SortedSet<String> keys = new TreeSet<String>(comparator);
        keys.addAll(set);
        return keys;
    }

    void writeMessageIds(final Document document, final Node newNode, final HashMap<String, Element> messageIdFieldMap) {
        final Element messagesNewTag = document.createElement(XMLNames.MESSAGES);
        final Set<String> messageIds = messageIdFieldMap.keySet();
        for (String messageId : messageIds) {
            final Element messTag = document.createElement(XMLNames.MESSAGE);
            messTag.setAttribute(XMLNames.ID, messageId);
            final String presence = messageIdFieldMap.get(messageId).getAttribute(XMLNames.PRESENCE);
            messTag.setAttribute(XMLNames.PRESENCE, presence);
            messagesNewTag.appendChild(messTag);
        }
        newNode.appendChild(messagesNewTag);
    }

    void readAndWriteElements(final Document document, final Element newField, final HashMap<String, Element> fields, final String elementsName, final String elementName) {
        final Element newElementsTag = document.createElement(elementsName);
        final HashMap<String, HashMap<String, Element>> subFieldsMap = new HashMap<String, HashMap<String, Element>>();  //HashMap<SubFieldNumber, HashMap<MessageId, SubField>>
        final Set<String> messageIds = fields.keySet();
        for (String messageId : messageIds) {
            final Element element = fields.get(messageId);
            final NodeList elementsList = element.getElementsByTagName(elementsName);
            if (elementsList.getLength() > 0) {
                Element elements = (Element) elementsList.item(0);
                final NodeList elementList = elements.getElementsByTagName(elementName);
                readElement(subFieldsMap, elementList, messageId);
            }
        }
        writeElements(document, subFieldsMap, newElementsTag, elementName);
        newField.appendChild(newElementsTag);
    }

    void writeElements(final Document document, final HashMap<String, HashMap<String, Element>> fieldsMap, final Node newNode, final String elementName) {
        final SortedSet<String> keys = createNumberNumberComparator(fieldsMap.keySet());
        for (final String key : keys) {
            final HashMap<String, Element> elements = fieldsMap.get(key);
            final Element newElement = document.createElement(elementName);
            writeFieldNumberFormat(elements.values().iterator().next(), newElement);
            newNode.appendChild(newElement);
            writeMessageIds(document, newElement, elements);
        }
    }

    public String getCurrentXml(final Document document, final String encoding) throws TransformerException {
        final TransformerFactory transfac = TransformerFactory.newInstance();
        transfac.setAttribute("indent-number", new Integer(4));
        final Transformer trans = transfac.newTransformer();
//        trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
        trans.setOutputProperty(OutputKeys.INDENT, "yes");
        trans.setOutputProperty(OutputKeys.ENCODING, encoding);
        final StringWriter sw = new StringWriter();
        final StreamResult result = new StreamResult(sw);
        final DOMSource source = new DOMSource(document);
        trans.transform(source, result);
        return sw.toString();
    }

    public static void main(String[] args) {
        try {
            final FieldsXmlCreator fieldsXmlCreator = new FieldsXmlCreator();
            fieldsXmlCreator.fieldsXmlCreate(EquensFileNames.FIRST_DOC, EquensFileNames.FIELDS_DOC);
        } catch (ParserConfigurationException e) {
            Log.out(e);
        } catch (SAXException e) {
            Log.out(e);
        } catch (IOException e) {
            Log.out(e);
        } catch (XMLException e) {
            Log.out(e);
        } catch (TransformerException e) {
            Log.out(e);
        } catch (Throwable t) {
            Log.out(t);
        }

    }
}

