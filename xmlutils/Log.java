package mtk.xmlutils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Log {
    private static final Logger LOG = LoggerFactory
            .getLogger(Log.class);

    public static void out(Throwable e) {
        LOG.error(e.getMessage(), e);
    }

    public static void out(String message) {
        LOG.debug(message);
    }

}
