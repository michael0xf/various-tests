package mtk.xmlutils;

import mtk.xmlcash.EquensFileNames;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.util.HashSet;

/**
 * Created with IntelliJ IDEA.
 * User: michaelk
 * Date: 11/13/13
 * Time: 7:28 AM
 * To change this template use File | Settings | File Templates.
 */
//This utility is not used directly in simulator. It is used to create XML.
public class PresencesXmlCreator extends MessagesXmlCreator {


    void presencesXmlCreate(final String srcFile, final String dstFile) throws ParserConfigurationException, SAXException, IOException, XMLException, TransformerException {
        super.messagesXmlCreate(srcFile, dstFile);
    }

    @Override
    void writeNormalizeDocument(Document oldDocument, final String encoding, final String xmlVersion, final String dstFile) throws TransformerException, ParserConfigurationException, IOException, XMLException {
        final Document newDocument = newDocument();
        newDocument.setXmlVersion(xmlVersion);
        writeMessages(oldDocument, newDocument);
        final String xml = getCurrentXml(newDocument, encoding);
        writeFile(dstFile, xml, encoding);
    }

    @Override
    void writeFields(final Element oldMessage, final Document newDocument, final Element newMessage) throws XMLException {
        NodeList nList = oldMessage.getElementsByTagName(XMLNames.FIELDS);
        if (nList.getLength() != 1)
            throw new XMLException("invalid count of bodies in message body " + oldMessage.getAttribute(XMLNames.ID));
        final Element newPresences = newDocument.createElement(XMLNames.PRESENCES);
        newMessage.appendChild(newPresences);
        final HashSet<String> presences = new HashSet<String>();
        readPresencesFromField((Element) nList.item(0), presences);
        writePresences(newDocument, newPresences, presences);
    }

    void writePresences(final Document newDocument, final Element newPresences, final HashSet<String> presences) throws XMLException {
        for (String presence : presences) {
            final Element newPresence = newDocument.createElement(XMLNames.PRESENCE);
            newPresence.setAttribute(XMLNames.VALUE, presence);
            newPresences.appendChild(newPresence);
        }
    }

    void readPresencesFromField(final Element oldFields, final HashSet<String> presences) throws XMLException {
        final NodeList nList = oldFields.getElementsByTagName(XMLNames.FIELD);
        for (int temp = 0; temp < nList.getLength(); temp++) {
            final Node nNode = nList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                final Element oldField = (Element) nNode;
                readPresence(oldField, presences);
                readPresencesFromElements(oldField, presences, XMLNames.SUBFIELDS, XMLNames.SUBFIELD);
                readPresencesFromElements(oldField, presences, XMLNames.TAGS, XMLNames.TAG);
            }
        }
    }

    void readPresencesFromElements(final Element element, final HashSet<String> presences, final String elementsName, final String elementName) throws XMLException {
        final NodeList nList = element.getElementsByTagName(elementsName);
        if (nList.getLength() == 1) {
            readPresencesFromElement((Element) nList.item(0), presences, elementName);
        } else if (nList.getLength() > 1)
            throw new XMLException("invalid count of bodies in field body " + element.getAttribute(elementsName));

    }

    void readPresencesFromElement(final Element oldElements, final HashSet<String> presences, final String elementName) throws XMLException {
        final NodeList nList = oldElements.getElementsByTagName(elementName);
        for (int temp = 0; temp < nList.getLength(); temp++) {
            final Node nNode = nList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                readPresence((Element) nNode, presences);
            }
        }
    }

    void readPresence(final Element field, final HashSet<String> presences) throws XMLException {
        final String presence = field.getAttribute(XMLNames.PRESENCE);
        if (!presences.contains(presence))
            presences.add(presence);
    }

    public static void main(String[] args) {
        try {
            final PresencesXmlCreator presencesXmlCreator = new PresencesXmlCreator();
            presencesXmlCreator.presencesXmlCreate(EquensFileNames.FIRST_DOC, EquensFileNames.PRESENCES_DOC);
        } catch (ParserConfigurationException e) {
            Log.out(e);
        } catch (SAXException e) {
            Log.out(e);
        } catch (IOException e) {
            Log.out(e);
        } catch (XMLException e) {
            Log.out(e);
        } catch (TransformerException e) {
            Log.out(e);
        } catch (Throwable t) {
            Log.out(t);
        }

    }


}
