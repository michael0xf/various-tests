package mtk.xmlutils;

import mtk.xmlcash.EquensFileNames;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: michaelk
 * Date: 11/12/13
 * Time: 10:19 AM
 * To change this template use File | Settings | File Templates.
 */

//This utility is not used directly in simulator. It is used to create XML.
public class ConnectorXmlCreator extends MessagesXmlCreator {


    void connectorXmlCreate(final String srcFile, final String dstFile) throws ParserConfigurationException, SAXException, IOException, XMLException, TransformerException {
        super.messagesXmlCreate(srcFile, dstFile);
    }

    @Override
    void writeNormalizeDocument(Document oldDocument, final String encoding, final String xmlVersion, final String dstFile) throws TransformerException, ParserConfigurationException, IOException, XMLException {
        final Document newDocument = newDocument();
        newDocument.setXmlVersion(xmlVersion);
        writeMessages(oldDocument, newDocument);
        final String xml = getCurrentXml(newDocument, encoding);
        writeFile(dstFile, xml, encoding);
    }

    @Override
    void writeFieldAttributes(final Element oldField, final Element newField) {
        super.writeFieldNumberFormat(oldField, newField);
    }

    public static void main(String[] args) {
        try {
            final ConnectorXmlCreator connectorXmlCreator = new ConnectorXmlCreator();
            connectorXmlCreator.connectorXmlCreate(EquensFileNames.FIRST_DOC, EquensFileNames.CONNECTOR_DOC);
        } catch (ParserConfigurationException e) {
            Log.out(e);
        } catch (SAXException e) {
            Log.out(e);
        } catch (IOException e) {
            Log.out(e);
        } catch (XMLException e) {
            Log.out(e);
        } catch (TransformerException e) {
            Log.out(e);
        } catch (Throwable t) {
            Log.out(t);
        }

    }


}
