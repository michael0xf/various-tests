package mtk.xmlutils;

import mtk.xmlcash.EMessageTypeEquens;
import mtk.xmlcash.EquensFileNames;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: michaelk
 * Date: 11/13/13
 * Time: 8:06 AM
 * To change this template use File | Settings | File Templates.
 */
//This utility is not used directly in simulator. It is used to create XML.
public class ProcessesXmlCreator extends PresencesXmlCreator {
    public final static int REQUEST_INDEX = 0;
    public final static int RESPONSE_INDEX = 1;
    public final static String SPLITTER = "_";


    void processesXmlCreate(final String[][] processes, final String srcFile, final String dstFile) throws ParserConfigurationException, SAXException, IOException, XMLException, TransformerException {
        final File fXmlFile = new File(srcFile);
        final DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        final DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        final Document oldDocument = dBuilder.parse(fXmlFile);
        oldDocument.getDocumentElement().normalize();
        writeProcessesDocument(processes, oldDocument, dstFile);
    }

    void readMessages(final Document oldDocument, final HashMap<String, Element> messages) throws XMLException {
        NodeList oldMessages = oldDocument.getElementsByTagName(XMLNames.MESSAGES);
        if (oldMessages.getLength() == 1)
            readMessage((Element) oldMessages.item(0), messages);
        else
            throw new XMLException("invalid count of bodies in root element");
    }

    public void readMessage(final Element oldMessages, final HashMap<String, Element> messages) throws XMLException {
        final NodeList oldMessageList = oldMessages.getElementsByTagName(XMLNames.MESSAGE);
        for (int temp = 0; temp < oldMessageList.getLength(); temp++) {
            final Node nNode = oldMessageList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                final Element oldMessage = (Element) nNode;
                messages.put(oldMessage.getAttribute(XMLNames.ID), oldMessage);
            }
        }
    }

    void writeProcessesDocument(final String[][] processes, final Document oldDocument, final String dstFile) throws ParserConfigurationException, TransformerException, IOException, XMLException {
        final Document newDocument = newDocument();
        newDocument.setXmlVersion(oldDocument.getXmlVersion());
        writeProcesses(processes, oldDocument, newDocument);
        final String encoding = oldDocument.getXmlEncoding();
        final String xml = getCurrentXml(newDocument, encoding);
        writeFile(dstFile, xml, encoding);
    }

    void writeProcesses(final String[][] processes, final Document oldDocument, final Document newDocument) throws XMLException {
        final HashMap<String, Element> messages = new HashMap<String, Element>();
        readMessages(oldDocument, messages);
        final Element processesTag = newDocument.createElement(XMLNames.PROCESSES);
        newDocument.appendChild(processesTag);
        for (String[] process : processes) {
            final String requestId = process[REQUEST_INDEX];
            final String responseId = process[RESPONSE_INDEX];
            writeProcessElement(messages, requestId, responseId, newDocument, processesTag);
        }
    }

    void writeProcessElement(final HashMap<String, Element> messages, final String requestId, final String responseId, Document newDocument, Element processesTag) throws XMLException {
        final Element newProcess = newDocument.createElement(XMLNames.PROCESS);
        newProcess.setAttribute(XMLNames.REQUEST_ID, requestId);
        newProcess.setAttribute(XMLNames.RESPONSE_ID, responseId);
        processesTag.appendChild(newProcess);
        writeProcess(messages.get(requestId), messages.get(responseId), newDocument, newProcess);
    }

    int calcCompareNumber(final String fieldNumber) {
        String[] p = fieldNumber.split(SPLITTER);
        int ret = Integer.parseInt(p[0]) << 16;
        if (p.length == 2) {
            ret += Integer.parseInt(p[1], 16) + 1;
        }
        return ret;
    }

    @Override
    SortedSet<String> createNumberNumberComparator(Set<String> set) {
        final Comparator<String> comparator = new Comparator<String>() {
            public int compare(String o1, String o2) {
                int i1 = calcCompareNumber(o1);
                int i2 = calcCompareNumber(o2);
                return o1.compareTo(o2);
            }
        };
        final SortedSet<String> keys = new TreeSet<String>(comparator);
        keys.addAll(set);
        return keys;
    }

    void writeProcess(final Element request, final Element response, final Document newDocument, final Element newProcess) throws XMLException {
        final HashMap<String, String> requestPresencesMap = new HashMap<String, String>(); // Number : Presence
        final HashMap<String, String> responsePresencesMap = new HashMap<String, String>(); // Number : Presence
        final HashSet<String> accordances = new HashSet<String>(); // Number : Presence
        readPresencesFromFields(request, response, requestPresencesMap, responsePresencesMap);
        final SortedSet<String> requestSet = createNumberNumberComparator(requestPresencesMap.keySet());
        final Element newFields = newDocument.createElement(XMLNames.FIELDS);
        newProcess.appendChild(newFields);
        writeRequestFields(accordances, requestSet, requestPresencesMap, responsePresencesMap, newDocument, newFields);
        writeRemainFields(accordances, requestSet, requestPresencesMap, responsePresencesMap, newDocument, newFields);
        writeAccordance(accordances, newDocument, newProcess);
    }

    void writeRequestFields(final HashSet<String> accordances, final SortedSet<String> requestSet, HashMap<String, String> requestPresencesMap, HashMap<String, String> responsePresencesMap, final Document newDocument, final Element newProcess) {
        for (String number : requestSet) {
            final String requestPresence = calcPresence(requestPresencesMap.get(number));
            final String responsePresence = calcPresence(responsePresencesMap.get(number));
            writeField(requestPresence, responsePresence, newDocument, number, newProcess);
            final String accordance = requestPresence + SPLITTER + responsePresence;
            if (!accordances.contains(accordance))
                accordances.add(accordance);
        }

    }

    void writeRemainFields(final HashSet<String> accordances, final SortedSet<String> requestSet, HashMap<String, String> requestPresencesMap, HashMap<String, String> responsePresencesMap, final Document newDocument, final Element newProcess) {
        final SortedSet<String> reponseSet = createNumberNumberComparator(responsePresencesMap.keySet());
        for (String number : reponseSet) {
            if (!requestSet.contains(number)) {
                final String requestPresence = calcPresence(requestPresencesMap.get(number));
                final String responsePresence = calcPresence(responsePresencesMap.get(number));
                writeField(requestPresence, responsePresence, newDocument, number, newProcess);
                final String accordance = requestPresence + SPLITTER + responsePresence;
                if (!accordances.contains(accordance))
                    accordances.add(accordance);
            }
        }
    }

    String calcPresence(final String presence) {
        if (presence == null)
            return XMLNames.NEVER;
        else if (presence == "")
            return "O";
        return presence;
    }

    void writeAccordance(final HashSet<String> accordances, final Document newDocument, final Element newProcess) {
        final Element newAccordances = newDocument.createElement(XMLNames.ECHOES);
        newProcess.appendChild(newAccordances);
        for (String accordance : accordances) {
            final Element newAccordance = newDocument.createElement(XMLNames.ECHO);
            newAccordance.setAttribute(XMLNames.VALUE, accordance);
            newAccordances.appendChild(newAccordance);
        }
    }

    void writeField(final String requestPresence, final String responsePresence, final Document newDocument, final String number, final Element newProcess) {
        final Element newField = newDocument.createElement(XMLNames.FIELD);
        newField.setAttribute(XMLNames.NAME, number);
        newField.setAttribute(XMLNames.ECHO, requestPresence + SPLITTER + responsePresence);
        newProcess.appendChild(newField);
    }

    void readPresencesFromFields(final Element request, final Element response, final HashMap<String, String> requestPresencesMap, final HashMap<String, String> responsePresencesMap) throws XMLException {
        NodeList nList = request.getElementsByTagName(XMLNames.FIELDS);
        if (nList.getLength() != 1)
            throw new XMLException("invalid count of bodies in message body " + request.getAttribute(XMLNames.ID));
        readPresencesFromField((Element) nList.item(0), requestPresencesMap);
        nList = response.getElementsByTagName(XMLNames.FIELDS);
        if (nList.getLength() != 1)
            throw new XMLException("invalid count of bodies in message body " + response.getAttribute(XMLNames.ID));
        readPresencesFromField((Element) nList.item(0), responsePresencesMap);
    }


    void readPresencesFromField(final Element oldFields, final HashMap<String, String> numberPresencesMap) throws XMLException {
        final NodeList nList = oldFields.getElementsByTagName(XMLNames.FIELD);
        for (int temp = 0; temp < nList.getLength(); temp++) {
            final Node nNode = nList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                final Element oldField = (Element) nNode;
                final String presence = oldField.getAttribute(XMLNames.PRESENCE);
                numberPresencesMap.put(oldField.getAttribute(XMLNames.NAME), presence);
                readPresencesFromElements(oldField, numberPresencesMap, XMLNames.SUBFIELDS, XMLNames.SUBFIELD);
                readPresencesFromElements(oldField, numberPresencesMap, XMLNames.TAGS, XMLNames.TAG);
            }
        }
    }


    void readPresencesFromElements(final Element oldElement, final HashMap<String, String> numberPresencesMap, final String elementsName, final String elementName) throws XMLException {
        final NodeList nList = oldElement.getElementsByTagName(elementsName);
        if (nList.getLength() == 1) {
            readPresencesFromElement(oldElement, (Element) nList.item(0), numberPresencesMap, elementName);
        } else if (nList.getLength() > 1)
            throw new XMLException("invalid count of bodies in field body " + oldElement.getAttribute(XMLNames.NAME));
    }

    void readPresencesFromElement(final Element oldElement, final Element oldElements, final HashMap<String, String> numberPresencesMap, final String elementName) throws XMLException {
        final NodeList nList = oldElements.getElementsByTagName(elementName);
        for (int temp = 0; temp < nList.getLength(); temp++) {
            final Node nNode = nList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) nNode;
                final String presence = element.getAttribute(XMLNames.PRESENCE);
                numberPresencesMap.put(oldElement.getAttribute(XMLNames.NAME) + SPLITTER + element.getAttribute(XMLNames.NAME), presence);
            }
        }
    }

    public static void main(String[] args) {
        try {
            final ProcessesXmlCreator processesXmlCreator = new ProcessesXmlCreator();
            processesXmlCreator.processesXmlCreate(new String[][]{

                    {EMessageTypeEquens.AUTHORIZATION_REQUEST.getCode(), EMessageTypeEquens.AUTHORIZATION_RESPONSE.getCode()},
                    {EMessageTypeEquens.INDOOR_FINANCIAL_REQUEST.getCode(), EMessageTypeEquens.INDOOR_FINANCIAL_RESPONSE.getCode()},
                    {EMessageTypeEquens.INDOOR_REVERSAL_ADVICE_REQUEST.getCode(), EMessageTypeEquens.INDOOR_REVERSAL_ADVICE_RESPONSE.getCode()},
                    {EMessageTypeEquens.OUTDOOR_FINANCIAL_REQUEST.getCode(), EMessageTypeEquens.OUTDOOR_FINANCIAL_RESPONSE.getCode()},
                    {EMessageTypeEquens.OUTDOOR_REVERSAL_ADVICE_REQUEST.getCode(), EMessageTypeEquens.OUTDOOR_REVERSAL_ADVICE_RESPONSE.getCode()},
                    {EMessageTypeEquens.NETWORK_MANAGEMENT_ADVICE_REQUEST.getCode(), EMessageTypeEquens.NETWORK_MANAGEMENT_ADVICE_RESPONSE.getCode()},
                    {EMessageTypeEquens.RECONCILIATION_ADVICE_REQUEST.getCode(), EMessageTypeEquens.RECONCILIATION_ADVICE_RESPONSE.getCode()},},
                    EquensFileNames.FIRST_DOC, EquensFileNames.PROCESSES_DOC);
        } catch (ParserConfigurationException e) {
            Log.out(e);
        } catch (SAXException e) {
            Log.out(e);
        } catch (IOException e) {
            Log.out(e);
        } catch (XMLException e) {
            Log.out(e);
        } catch (TransformerException e) {
            Log.out(e);
        } catch (Throwable t) {
            Log.out(t);
        }

    }

}
