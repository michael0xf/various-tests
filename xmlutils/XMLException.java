package mtk.xmlutils;

import com.payplaza.pms.commons.exceptions.ServiceException;

/**
 * Created with IntelliJ IDEA.
 * User: michaelk
 * Date: 11/11/13
 * Time: 8:40 AM
 * To change this template use File | Settings | File Templates.
 */
public class XMLException extends ServiceException {
    public XMLException(String message) {
        super(message);
    }
}