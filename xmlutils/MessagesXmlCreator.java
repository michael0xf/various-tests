package mtk.xmlutils;

import mtk.xmlcash.EquensFileNames;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: michaelk
 * Date: 11/12/13
 * Time: 10:19 AM
 * To change this template use File | Settings | File Templates.
 */
//This utility is not used directly in simulator. It is used to create XML.
public class MessagesXmlCreator extends FieldsXmlCreator {

    void messagesXmlCreate(final String srcFile, final String dstFile) throws ParserConfigurationException, SAXException, IOException, XMLException, TransformerException {
        final File fXmlFile = new File(srcFile);
        final DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        final DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        final Document doc = dBuilder.parse(fXmlFile);
        doc.getDocumentElement().normalize();
        Log.out("Root element :" + doc.getDocumentElement().getNodeName());
        writeNormalizeDocument(doc, doc.getXmlEncoding(), doc.getXmlVersion(), dstFile);
    }

    void writeNormalizeDocument(Document oldDocument, final String encoding, final String xmlVersion, final String dstFile) throws TransformerException, ParserConfigurationException, IOException, XMLException {
        final Document newDocument = newDocument();
        newDocument.setXmlVersion(xmlVersion);
        writeMessages(oldDocument, newDocument);
        final String xml = getCurrentXml(newDocument, encoding);
        writeFile(dstFile, xml, encoding);
    }

    void writeMessages(final Document oldDocument, final Document newDocument) throws XMLException {
        NodeList oldMessages = oldDocument.getElementsByTagName(XMLNames.MESSAGES);
        final Element newMessages = newDocument.createElement(XMLNames.MESSAGES);
        newDocument.appendChild(newMessages);
        if (oldMessages.getLength() == 1)
            writeMessage((Element) oldMessages.item(0), newDocument, newMessages);
        else
            throw new XMLException("invalid count of bodies in root element");
    }

    public void writeMessage(final Element oldMessages, final Document newDocument, final Element newMessages) throws XMLException {
        final NodeList oldMessageList = oldMessages.getElementsByTagName(XMLNames.MESSAGE);
        for (int temp = 0; temp < oldMessageList.getLength(); temp++) {
            final Node nNode = oldMessageList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                final Element newMessage = newDocument.createElement(XMLNames.MESSAGE);
                newMessages.appendChild(newMessage);
                final Element oldMessage = (Element) nNode;
                newMessage.setAttribute(XMLNames.ID, oldMessage.getAttribute(XMLNames.ID));
                newMessage.setAttribute(XMLNames.TYPE, oldMessage.getAttribute(XMLNames.TYPE));
                writeFields(oldMessage, newDocument, newMessage);
            }
        }
    }

    void writeFields(final Element oldMessage, final Document newDocument, final Element newMessage) throws XMLException {
        NodeList nList = oldMessage.getElementsByTagName(XMLNames.FIELDS);
        if (nList.getLength() != 1)
            throw new XMLException("invalid count of bodies in message body " + oldMessage.getAttribute(XMLNames.ID));
        final Element newFields = newDocument.createElement(XMLNames.FIELDS);
        newMessage.appendChild(newFields);
        writeField((Element) nList.item(0), newDocument, newFields);

    }

    void writeField(final Element oldFields, final Document newDocument, final Element newFields) throws XMLException {
        final NodeList nList = oldFields.getElementsByTagName(XMLNames.FIELD);
        for (int temp = 0; temp < nList.getLength(); temp++) {
            final Node nNode = nList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                final Element newField = newDocument.createElement(XMLNames.FIELD);
                newFields.appendChild(newField);
                final Element oldField = (Element) nNode;
                writeFieldAttributes(oldField, newField);
                writeElements(oldField, newDocument, newField, XMLNames.SUBFIELDS, XMLNames.SUBFIELD);
                writeElements(oldField, newDocument, newField, XMLNames.TAGS, XMLNames.TAG);
            }
        }
    }

    void writeElements(final Element oldElement, final Document newDocument, final Element newField, final String elementsName, final String elementName) throws XMLException {
        final NodeList nList = oldElement.getElementsByTagName(elementsName);
        if (nList.getLength() == 1) {
            final Element elements = newDocument.createElement(elementsName);
            newField.appendChild(elements);
            writeElement((Element) nList.item(0), newDocument, elements, elementName);
        } else if (nList.getLength() > 1)
            throw new XMLException("invalid count of bodies in field body " + oldElement.getAttribute(XMLNames.NAME));

    }

    void writeElement(final Element oldElements, final Document newDocument, final Element newElements, final String elementName) throws XMLException {
        final NodeList nList = oldElements.getElementsByTagName(elementName);
        for (int temp = 0; temp < nList.getLength(); temp++) {
            final Node nNode = nList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                final Element oldElement = (Element) nNode;
                final Element newElement = newDocument.createElement(elementName);
                newElements.appendChild(newElement);
                writeFieldAttributes(oldElement, newElement);
            }
        }
    }


    void writeFieldAttributes(final Element oldField, final Element newField) {
        newField.setAttribute(XMLNames.NAME, oldField.getAttribute(XMLNames.NAME));
        newField.setAttribute(XMLNames.PRESENCE, oldField.getAttribute(XMLNames.PRESENCE));
    }

    public static void main(String[] args) {
        try {
            final MessagesXmlCreator messagesXmlCreator = new MessagesXmlCreator();
            messagesXmlCreator.messagesXmlCreate(EquensFileNames.FIRST_DOC, EquensFileNames.MESSAGES_DOC);
        } catch (ParserConfigurationException e) {
            Log.out(e);
        } catch (SAXException e) {
            Log.out(e);
        } catch (IOException e) {
            Log.out(e);
        } catch (XMLException e) {
            Log.out(e);
        } catch (TransformerException e) {
            Log.out(e);
        } catch (Throwable t) {
            Log.out(t);
        }

    }


}
