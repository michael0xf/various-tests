CREATE DATABASE db

-- Table: public.account

-- DROP TABLE public.account;

CREATE TABLE public.account
(
  id bigint NOT NULL,
  created_at timestamp without time zone NOT NULL,
  updated_at timestamp without time zone NOT NULL,
  email character varying(100),
  name character varying(100),
  CONSTRAINT account_pkey PRIMARY KEY (id),
  CONSTRAINT uk_bb9lrmwswqvhcy1y430ki00ir UNIQUE (name),
  CONSTRAINT uk_q0uja26qgu1atulenwup9rxyr UNIQUE (email)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.account
  OWNER TO postgres;


-- Table: public.field

-- DROP TABLE public.field;

CREATE TABLE public.field
(
  id bigint NOT NULL,
  created_at timestamp without time zone NOT NULL,
  updated_at timestamp without time zone NOT NULL,
  lat real NOT NULL,
  lon real NOT NULL,
  name character varying(100),
  account_id bigint NOT NULL,
  CONSTRAINT field_pkey PRIMARY KEY (id),
  CONSTRAINT fkbvx6lf3i2411uqoksvfj13wv6 FOREIGN KEY (account_id)
      REFERENCES public.account (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.field
  OWNER TO postgres;


