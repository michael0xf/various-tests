package mtk.test.springboot.controller;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NewFieldResponse {
    @JsonProperty
    long FieldId;
    @JsonProperty
    long AccountId;
    public NewFieldResponse(long FieldId, long AccountId) {
        this.FieldId = FieldId;
        this.AccountId = AccountId;
    }

}
