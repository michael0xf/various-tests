package mtk.test.springboot.controller;

import com.fasterxml.jackson.annotation.JsonProperty;
import mtk.test.springboot.model.Field;

public class FieldResponse extends PutFieldRequest{
    @JsonProperty
    long FieldId;
    public FieldResponse(Field field) {
        super(field.getLat(),
                field.getLon(),
                field.getName(),
                field.getAccount().getName(),
                field.getAccount().getEmail());
        this.FieldId = field.getId();
    }

}
