package mtk.test.springboot.controller;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PutFieldRequest {
    @JsonProperty
    Float Lat;
    @JsonProperty
    Float Lon;
    @JsonProperty
    String FieldName;
    @JsonProperty
    String AccountName;
    @JsonProperty
    String AccountEmail;

    public PutFieldRequest(@JsonProperty("Lat")Float Lat,
                           @JsonProperty("Lon")Float Lon,
                           @JsonProperty("FieldName")String FieldName,
                           @JsonProperty("AccountName")String AccountName,
                           @JsonProperty("AccountEmail")String AccountEmail) {
        this.Lat = Lat;
        this.Lon = Lon;
        this.FieldName = FieldName;
        this.AccountName = AccountName;
        this.AccountEmail = AccountEmail;
    }
}
