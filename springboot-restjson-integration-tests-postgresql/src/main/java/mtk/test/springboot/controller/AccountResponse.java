package mtk.test.springboot.controller;

import com.fasterxml.jackson.annotation.JsonProperty;
import mtk.test.springboot.model.Account;
import mtk.test.springboot.model.Field;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

public class AccountResponse {

    @JsonProperty
    Collection<Long> Fields;

    @JsonProperty
    long AccountId;

    @JsonProperty
    String Name;

    @JsonProperty
    String Email;

    public AccountResponse(Account account) {
        Collection<Field> fields = account.getFields();
        AccountId = account.getId();
        Name = account.getName();
        Email = account.getEmail();
        if (fields.size() > 0) {
            ArrayList<Long> list = new ArrayList(fields.size());
            for(Field f: fields){
                list.add(f.getId());
            }
            Fields = list;
        }
    }
}
