package mtk.test.springboot.controller;

import java.util.ArrayList;
import java.util.List;

import mtk.test.springboot.model.Account;
import mtk.test.springboot.model.Field;
import mtk.test.springboot.model.FieldsTable;
import mtk.test.springboot.service.AccountService;
import mtk.test.springboot.service.FieldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.hibernate.internal.util.collections.CollectionHelper.isEmpty;

/**
 * Spring Boot приложение для получения/хранения данных о поле и его владельце.
 * Ошибки обращения должны возвращаться в виде:
 * {
 *  "Error": "field.does.not.exist",
 *  "Code": 5,
 *  "Description": "Field does not exist"
 * }
 * see mtk.test.springboot.exceptionExceptionResponseAndErrorCodes
 *
 * @author <a href="mailto:mtkravchenko@gmail.com">Mikhail Kravchenko</a>
 * @version 1.0
 */

@RestController
@RequestMapping
public class FieldController {


    @Autowired
    private FieldService fieldService;

    @Autowired
    private AccountService accountService;


    /**
     * PUT /api/field – добавление нового поля.
     * На вход поступают данные в формате JSON:
     * {
     *  "Lat": 23.9948834,
     *  "Lon": -44.243535,
     *  "FieldName": "My field number 1",
     *  "AccountName": "User1",
     *  "AccountEmail": "user1@mail.com"
     * }
     * <p>
     * Принимаем, что AccountName уникален.
     * На выходе должны получить данные об идентификаторах поля и пользователя в формате JSON:
     * {
     *  "FieldId": 133312,
     *  "AccountId": 23424
     * }
     */
    @PutMapping("/api/field")
    public ResponseEntity<NewFieldResponse> putField(@RequestBody PutFieldRequest putRequest) {
        Field field = fieldService.createField(
                putRequest.Lat,
                putRequest.Lon,
                putRequest.FieldName,
                putRequest.AccountName,
                putRequest.AccountEmail);
        NewFieldResponse newFieldResponse = new NewFieldResponse(field.getId(), field.getAccount().getId());
        return new ResponseEntity(newFieldResponse, HttpStatus.CREATED);

    }


    /**
     * POST /api/field/<field_id> – обновление данных поля.
     * На вход поступают данные в формате JSON:
     * {
     *  "Lat": 23.9948834,
     *  "Lon": -44.243535,
     *  "FieldName": "My field number 1",
     *  "AccountName": "User1",
     *  "AccountEmapi": "user1@mail.com"
     * }
     */
    @PostMapping("/api/field/{fieldId}")
    public ResponseEntity<Void> postField(@PathVariable long fieldId,
                                          @RequestBody PutFieldRequest putRequest) {


        fieldService.updateField(fieldId,
                putRequest.Lat,
                putRequest.Lon,
                putRequest.FieldName,
                putRequest.AccountName,
                putRequest.AccountEmail);
        return ResponseEntity.ok().build();
    }

    /**
     * DELETE /api/field/<field_id> - удаление поля.
     */
    @DeleteMapping("/api/field/{fieldId}")
    public ResponseEntity<Void> deleteField(@PathVariable long fieldId) {
        fieldService.deleteField(fieldId);
        return ResponseEntity.ok().build();
    }


    /**
     * GET /api/field – получение данных о всех полях.
     * Данные получаем в формате JSON:
     * [
     *  {
     *      "FieldId": 133312,
     *      "Lat": 23.9948834,
     *      "Lon": -44.243535,
     *      "FieldName": "My field number 1",
     *      "AccountName": "User1",
     *      "AccountEmail": "user1@mail.com"
     *  }
     * ]
     */
    @GetMapping("/api/field")
    public ResponseEntity<List<FieldResponse>> getFields() {
        List<Field> list = fieldService.getFields();
        int size;
        if (isEmpty(list)) {
            size = 0;
        } else {
            size = list.size();
        }
        List<FieldResponse> ret = new ArrayList(size);
        for (Field field : list) {
            ret.add(new FieldResponse(field));
        }
        return new ResponseEntity((ret), HttpStatus.OK);
    }

    /**
     * GET /api/field/<field_id> – получение данных о конкретном поле
     * Данные получаем в формате JSON:
     * {
     *  "FieldId": 133312,
     *  "Lat": 23.9948834,
     *  "Lon": -44.243535,
     *  "FieldName": "My field number 1",
     *  "AccountName": "User1",
     *  "AccountEmail": "user1@mail.com"
     * }
     */
    @GetMapping("/api/field/{fieldId}")
    public ResponseEntity<FieldResponse> getField(@PathVariable long fieldId) {
        return new ResponseEntity(new FieldResponse(fieldService.findField(fieldId)), HttpStatus.OK);
    }

    /**
     * GET /api/account – получение данных обо всех аккаунтах
     * Данные получаем в формате JSON:
     * [
     *  {
     *      "AccountId": 23424
     *      "AccountName": "UserId",
     *      "AccountEmail": "user1@mail.com",
     *      "Fields": [
     *          "133312",
     *          "133313"
     *      ]
     *  }
     * ]
     */
    @GetMapping("/api/account")
    public ResponseEntity<List<AccountResponse>> getAccounts() {
        List<Account> list = accountService.retrieveAccounts();
        int size;
        if (isEmpty(list)) {
            size = 0;
        } else {
            size = list.size();
        }
        List<AccountResponse> ret = new ArrayList(size);
        for (Account account : list) {
            ret.add(new AccountResponse(account));
        }
        return new ResponseEntity((ret), HttpStatus.OK);
    }

    /**
     * GET /api/account/<account_id> – получение данных о конкретном аккаунте
     * Данные получаем в формате JSON:
     * {
     *  "AccountId": 23424
     *  "AccountName": "UserId",
     *  "AccountEmail": "user1@mail.com",
     *  "Fields": [
     *      "133312",
     *      "133313"
     *  ]
     * }
     */
    @GetMapping("/api/account/{accountId}")
    public ResponseEntity<AccountResponse> getAccount(@PathVariable long accountId) {
        return new ResponseEntity(new AccountResponse(accountService.retrieveAccountById(accountId)), HttpStatus.OK);
    }

    /**
     * GET / - простое табличное легко читаемое отображение списка полей, учесть что полей может быть очень много.
     */
    @GetMapping("/")
    public ResponseEntity<String> getFieldsTable() {

        List<Field> list = fieldService.getFields();
        if (isEmpty(list)) {
            return new ResponseEntity(FieldsTable.getHeader(), HttpStatus.OK);
        }
        return new ResponseEntity(FieldsTable.createTable(list), HttpStatus.OK);
    }


}
