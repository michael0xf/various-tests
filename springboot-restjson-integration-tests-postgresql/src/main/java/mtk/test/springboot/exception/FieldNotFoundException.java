package mtk.test.springboot.exception;

public class FieldNotFoundException extends IdNotFoundException {
    public FieldNotFoundException (long id) {
        super("Field not found", id);
    }

    @Override
    public void check(ExceptionResponseAndErrorCodes exceptionResponseAndErrorCodes) {
        exceptionResponseAndErrorCodes.visit(this);
    }
}
