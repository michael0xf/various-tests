package mtk.test.springboot.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RestController
public class CustomizedResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
  Logger logger = LoggerFactory.getLogger(CustomizedResponseEntityExceptionHandler.class);
  @ExceptionHandler(Exception.class)
  public final ResponseEntity<ExceptionResponseAndErrorCodes> handleAllExceptions(Exception ex, WebRequest request) {
    logger.error(ex.getMessage(), ex);
    ExceptionResponseAndErrorCodes exceptionResponseAndErrorCodes = new ExceptionResponseAndErrorCodes(ex, request);
    return new ResponseEntity<>(exceptionResponseAndErrorCodes, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(AccountNotFoundException.class)
  public final ResponseEntity<ExceptionResponseAndErrorCodes> handleResourceNotFoundException(AccountNotFoundException ex, WebRequest request) {
    logger.error(ex.getMessage(), ex);
    ExceptionResponseAndErrorCodes exceptionResponseAndErrorCodes = new ExceptionResponseAndErrorCodes(ex, request);
    return new ResponseEntity<>(exceptionResponseAndErrorCodes, HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler(FieldNotFoundException.class)
  public final ResponseEntity<ExceptionResponseAndErrorCodes> handleResourceNotFoundException(FieldNotFoundException ex, WebRequest request) {
    logger.error(ex.getMessage(), ex);
    ExceptionResponseAndErrorCodes exceptionResponseAndErrorCodes = new ExceptionResponseAndErrorCodes(ex, request);
    return new ResponseEntity<>(exceptionResponseAndErrorCodes, HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler(FieldAlreadyExistsException.class)
  public final ResponseEntity<ExceptionResponseAndErrorCodes> handleResourceNotFoundException(FieldAlreadyExistsException ex, WebRequest request) {
    logger.error(ex.getMessage(), ex);
    ExceptionResponseAndErrorCodes exceptionResponseAndErrorCodes = new ExceptionResponseAndErrorCodes(ex, request);
    return new ResponseEntity<>(exceptionResponseAndErrorCodes, HttpStatus.CONFLICT);
  }


  @ExceptionHandler(EmailAlreadyUsedException.class)
  public final ResponseEntity<ExceptionResponseAndErrorCodes> handleResourceNotFoundException(EmailAlreadyUsedException ex, WebRequest request) {
    logger.error(ex.getMessage(), ex);
    ExceptionResponseAndErrorCodes exceptionResponseAndErrorCodes = new ExceptionResponseAndErrorCodes(ex, request);
    return new ResponseEntity<>(exceptionResponseAndErrorCodes, HttpStatus.CONFLICT);
  }


  @ExceptionHandler(AccountLinkedToOtherEmailException.class)
  public final ResponseEntity<ExceptionResponseAndErrorCodes> handleResourceNotFoundException(AccountLinkedToOtherEmailException ex, WebRequest request) {
    logger.error(ex.getMessage(), ex);
    ExceptionResponseAndErrorCodes exceptionResponseAndErrorCodes = new ExceptionResponseAndErrorCodes(ex, request);
    return new ResponseEntity<>(exceptionResponseAndErrorCodes, HttpStatus.NOT_FOUND);
  }


  @ExceptionHandler(NameDoesNotMatchEmail.class)
  public final ResponseEntity<ExceptionResponseAndErrorCodes> handleResourceNotFoundException(NameDoesNotMatchEmail ex, WebRequest request) {
    logger.error(ex.getMessage(), ex);
    ExceptionResponseAndErrorCodes exceptionResponseAndErrorCodes = new ExceptionResponseAndErrorCodes(ex, request);
    return new ResponseEntity<>(exceptionResponseAndErrorCodes, HttpStatus.NOT_FOUND);
  }

}