package mtk.test.springboot.exception;

import mtk.test.springboot.model.Field;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class FieldAlreadyExistsException extends AgricultureException{
    private Field field;
    public FieldAlreadyExistsException(Field field) {
        super("Field " + field.getName() + " already exists for user " + field.getAccount().getName());
        setField(field);
    }

    @Override
    public void check(ExceptionResponseAndErrorCodes exceptionResponseAndErrorCodes) {
        exceptionResponseAndErrorCodes.visit(this);
    }

    public Field getField() {
        return field;
    }

    public void setField(Field field) {
        this.field = field;
    }


}
