package mtk.test.springboot.exception;

public abstract class AgricultureException extends RuntimeException{

    public AgricultureException(String message) {
        super(message);
    }

    abstract void check(ExceptionResponseAndErrorCodes exceptionResponseAndErrorCodes);
}
