package mtk.test.springboot.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public abstract class IdNotFoundException extends AgricultureException {
    private long id;

    public IdNotFoundException(String message, long id) {
        super(message);
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}
