package mtk.test.springboot.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class EmailAlreadyUsedException extends AgricultureException{
    private String email;
    public EmailAlreadyUsedException(String email) {
        super("Email already used: " + email);
        setEmail(email);
    }


    @Override
    public void check(ExceptionResponseAndErrorCodes exceptionResponseAndErrorCodes) {
        exceptionResponseAndErrorCodes.visit(this);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
