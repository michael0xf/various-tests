package mtk.test.springboot.exception;

public class NameDoesNotMatchEmail extends AgricultureException {
    private String name;
    private String email;

    public NameDoesNotMatchEmail(String name, String email) {
        super("Account " + name + " linked to other email and " + email + " also already used.");
        setName(name);
        setEmail(email);
    }


    @Override
    public void check(ExceptionResponseAndErrorCodes exceptionResponseAndErrorCodes) {
        exceptionResponseAndErrorCodes.visit(this);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
