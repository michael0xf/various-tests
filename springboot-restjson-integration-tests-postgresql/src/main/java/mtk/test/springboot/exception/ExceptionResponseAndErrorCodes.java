package mtk.test.springboot.exception;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.web.context.request.WebRequest;

public class ExceptionResponseAndErrorCodes {
  public final static int INTERNAL_EXCEPTION = 1;
  public final static int ACCOUNT_LINKED_TO_OTHER_EMAIL_EXCEPTION = 2;
  public final static int ACCOUNT_NOT_FOUND_EXCEPTION = 3;
  public final static int FIELD_NOT_FOUND_EXCEPTION = 4;
  public final static int FIELD_ALREADY_EXISTS_EXCEPTION = 5;
  public final static int EMAIL_ALREADY_USED_EXCEPTION = 6;
  public final static int NAME_DOES_NOT_MATCH_EMAIL = 7;

  @JsonProperty
  private String Error;
  @JsonProperty
  private int Code;
  @JsonProperty
  private String Description;

  public ExceptionResponseAndErrorCodes(Exception e, WebRequest request) {
    this.Error = e.getMessage();
    this.Code = INTERNAL_EXCEPTION;
    this.Description = request.getDescription(false);
  }

  public ExceptionResponseAndErrorCodes(AgricultureException e, WebRequest request) {
    this.Error = e.getMessage();
    this.Description = request.getDescription(false);
    e.check(this);
  }

  public void visit(FieldAlreadyExistsException e) {
    Code = FIELD_ALREADY_EXISTS_EXCEPTION;
  }

  public void visit(FieldNotFoundException e) {
    Code = FIELD_NOT_FOUND_EXCEPTION;
  }

  public void visit(AccountNotFoundException e) {
    Code = ACCOUNT_NOT_FOUND_EXCEPTION;
  }

  public void visit(EmailAlreadyUsedException e) {
    Code = EMAIL_ALREADY_USED_EXCEPTION;
  }

  public void visit(AccountLinkedToOtherEmailException e) {
    Code = ACCOUNT_LINKED_TO_OTHER_EMAIL_EXCEPTION;
  }

  public void visit(NameDoesNotMatchEmail e) {
    Code = NAME_DOES_NOT_MATCH_EMAIL;
  }
}