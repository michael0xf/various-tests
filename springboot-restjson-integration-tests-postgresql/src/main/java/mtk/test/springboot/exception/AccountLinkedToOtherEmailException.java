package mtk.test.springboot.exception;

public class AccountLinkedToOtherEmailException extends AgricultureException {
    private String name;

    public AccountLinkedToOtherEmailException(String name) {
        super("Account " + name + " linked to other email.");
        setName(name);
    }


    @Override
    public void check(ExceptionResponseAndErrorCodes exceptionResponseAndErrorCodes) {
        exceptionResponseAndErrorCodes.visit(this);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
