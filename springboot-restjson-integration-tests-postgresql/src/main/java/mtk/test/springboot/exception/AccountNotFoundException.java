package mtk.test.springboot.exception;

public class AccountNotFoundException extends IdNotFoundException {
    public AccountNotFoundException (long id) {
        super("Account not found", id);
    }

    @Override
    public void check(ExceptionResponseAndErrorCodes exceptionResponseAndErrorCodes) {
        exceptionResponseAndErrorCodes.visit(this);
    }

}
