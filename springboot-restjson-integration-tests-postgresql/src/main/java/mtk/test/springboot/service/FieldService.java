package mtk.test.springboot.service;

import java.util.*;

import mtk.test.springboot.exception.FieldAlreadyExistsException;
import mtk.test.springboot.exception.FieldNotFoundException;
import mtk.test.springboot.model.Account;
import mtk.test.springboot.model.Field;
import mtk.test.springboot.repository.FieldRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FieldService {

	@Autowired
	private FieldRepository fieldRepository;


	@Autowired
	private AccountService accountService;



	public List<Field> getFields() {
		return fieldRepository.findAll();
	}

	public Field findField(long fieldId) {
		return fieldRepository.findOne(fieldId);
	}


	public Field createField(Float lat,
							Float lon,
							String fieldName,
							String accountName,
							String accountEmail) {
		Account account = accountService.findOrCreateAccount(accountName, accountEmail);
		Field field = fieldRepository.findByNameAndAccountId(fieldName, account.getId());
		if (field != null){
			throw new FieldAlreadyExistsException(field);
		}
		field = new Field();
		field.setAccount(account);
		field.setName(fieldName);
		field.setLon(lon);
		field.setLat(lat);
		fieldRepository.save(field);
		return field;
	}

	public Field updateField(long fieldId, Float lat, Float lon, String fieldName, String accountName, String accountEmail) {
		Field field = fieldRepository.findOne(fieldId);
		if (field == null){
			throw new FieldNotFoundException(fieldId);
		}
		Account oldAccount = field.getAccount();
		int fieldsCount = oldAccount.getFields().size();
		Account account = accountService.updateAccount(oldAccount, accountName, accountEmail);
		field.setLat(lat);
		field.setLon(lon);
		field.setName(fieldName);
		field.setAccount(account);
		fieldRepository.save(field);
		if (!oldAccount.equals(account)){
			if (fieldsCount == 1){
				accountService.deleteAccount(oldAccount);
			}
		}
		return field;
	}

	public Field deleteField(long fieldId) {
		Field field = fieldRepository.findOne(fieldId);
		if (field == null){
			throw new FieldNotFoundException(fieldId);
		}
		fieldRepository.delete(field);
		return field;
	}
}