package mtk.test.springboot.service;

import java.util.*;

import mtk.test.springboot.exception.AccountLinkedToOtherEmailException;
import mtk.test.springboot.exception.EmailAlreadyUsedException;
import mtk.test.springboot.exception.NameDoesNotMatchEmail;
import mtk.test.springboot.model.Account;
import mtk.test.springboot.model.Field;
import mtk.test.springboot.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AccountService {

    @Autowired
    private FieldService fieldService;

    @Autowired
    private AccountRepository accountRepository;


    public Account retrieveAccountById(long accountId) {
        Account account = accountRepository.findOne(accountId);
        if (account == null) {
            return null; }

        return account;
    }

    public List<Account> retrieveAccounts() {
        return accountRepository.findAll();
    }

    public Account findOrCreateAccount(String accountName,
                                       String accountEmail) {
        Account accountByEmail = accountRepository.findByEmail(accountEmail);
        Account accountByName = accountRepository.findByName(accountName);
        if (accountByName == null) {
            if (accountByEmail == null) {
                accountByName = new Account();
                accountByName.setEmail(accountEmail);
                accountByName.setName(accountName);
                accountRepository.save(accountByName);
                return accountByName;
            }else{
                throw new EmailAlreadyUsedException(accountEmail);
            }
        }else{
            if (accountByEmail == null) {
                throw new AccountLinkedToOtherEmailException(accountName);
            }else{
                if (accountByName.equals(accountByEmail)){
                    return accountByName;
                }else{
                    throw new NameDoesNotMatchEmail(accountName, accountEmail);
                }
            }
        }


    }

    public Account updateAccount(Account account, String accountName, String accountEmail) {
        Account accountByEmail = accountRepository.findByEmail(accountEmail);
        Account accountByName = accountRepository.findByName(accountName);
        if (accountByName == null) {
            if (accountByEmail == null) {
                account.setEmail(accountEmail);
                account.setName(accountName);
                accountRepository.save(account);
                return account;
            }else{
                if (accountByEmail.equals(account)) {
                    account.setName(accountName);
                    accountRepository.save(account);
                    return account;
                }else {
                    throw new EmailAlreadyUsedException(accountEmail);
                }
            }
        }else{
            if (accountByEmail == null) {
                if (accountByName.equals(account)) {
                    account.setEmail(accountEmail);
                    accountRepository.save(account);
                    return account;
                }else {
                    throw new AccountLinkedToOtherEmailException(accountName);
                }
            }else{
                if (accountByName.equals(accountByEmail)){
                    return accountByName;
                }else{
                    throw new NameDoesNotMatchEmail(accountName, accountEmail);
                }
            }
        }
    }

    public void deleteAccount(Account oldAccount) {
        accountRepository.delete(oldAccount);
    }
}