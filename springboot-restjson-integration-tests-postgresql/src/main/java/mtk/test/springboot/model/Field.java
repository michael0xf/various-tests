package mtk.test.springboot.model;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = "field", schema="public")
public class Field extends Row{

    private float lat;
    private float lon;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Account account;

    @Size(min = 1, max = 100)
    private String name;

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLon() {
        return lon;
    }

    public void setLon(float lon) {
        this.lon = lon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
