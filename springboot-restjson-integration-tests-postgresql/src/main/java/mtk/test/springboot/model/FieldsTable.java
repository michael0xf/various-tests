package mtk.test.springboot.model;

import java.util.List;

import static org.aspectj.util.LangUtil.isEmpty;

public class FieldsTable {
    public enum Column{
        ID("Id", 7){
            @Override
            public String getCell(Field field) {
                return FieldsTable.getCell(getSize(), field.getId());
            }
        },
        FIELD_NAME("Field name", 18){
            @Override
            public String getCell(Field field) {
                return FieldsTable.getCell(getSize(), field.getName());
            }
        },
        LAT("Lat", 10){
            @Override
            public String getCell(Field field) {
                return FieldsTable.getCell(getSize(), field.getLat());
            }
        },
        LON("Lon", 10){
            @Override
            public String getCell(Field field) {
                return FieldsTable.getCell(getSize(), field.getLon());
            }
        },
        ACCOUNT("Account", 18){
            @Override
            public String getCell(Field field) {
                return FieldsTable.getCell(getSize(), field.getAccount().getName());
            }
        },
        EMAIL("Email", 18){
            @Override
            public String getCell(Field field) {
                return FieldsTable.getCell(getSize(), field.getAccount().getEmail());
            }
        };
        private int size;
        private String name;
        Column(String name, int size){
            this.size = size;
            this.name = "|" + center(name, size);
        }

        public static String center(String text, int len){
            String out = String.format("%" + len +"s%s%" + len + "s", "", text, "");
            float mid = (out.length() / 2);
            float start = mid - (len / 2);
            float end = start + len;
            return out.substring((int)start, (int)end);
        }
        public abstract String getCell(Field field);
        public String getColumnName(){ return name;}
        public int getSize() {
            return size;
        }
    }
    public static String getCell(int columnSize, Long decimal) {
       if (decimal == null){
            return "|?";
        }
        return String.format("|%" + columnSize + "d", decimal);
    }

    public static String getCell(int columnSize, Float value) {
        if (value == null){
            return "|?";
        }
        return String.format("|%" + columnSize + "f", value);
    }

    public static String getCell(int columnSize, String str) {
        if (isEmpty(str)){
            return "|?";
        }
        return String.format("|%" + columnSize + "s", str);
    }

    public static String getHeader(){
        StringBuilder stringBuilder = new StringBuilder();
        for(FieldsTable.Column column: Column.values()){
            stringBuilder.append(column.getColumnName());
        }
        stringBuilder.append('\n');
        return stringBuilder.toString();
    }
    public static String createTable(List<Field> fields){
        StringBuilder stringBuilder = new StringBuilder(getHeader());
        for(Field field: fields){
            for(Column column: Column.values()){
                stringBuilder.append(column.getCell(field));
            }
            stringBuilder.append('\n');
        }
        return stringBuilder.toString();
    }

}
