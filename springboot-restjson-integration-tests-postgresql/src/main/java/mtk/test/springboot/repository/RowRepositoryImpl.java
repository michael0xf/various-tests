package mtk.test.springboot.repository;

import mtk.test.springboot.model.Row;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Transactional(readOnly = true)
public abstract class RowRepositoryImpl< T extends Row> implements RowRepository<T> {

    private Class< T > clazz;


    @PersistenceContext
    private EntityManager entityManager;


    public EntityManager getEntityManager() {
        return entityManager;
    }

    public void setClazz( Class< T > clazzToSet ) {
        this.clazz = clazzToSet;
    }

    public T findOne( Long id ){
        try{
            return getEntityManager().find( clazz, id );
        }catch (NoResultException e){
            return null;
        }
    }
    public List< T > findAll(){
        try {
            return getEntityManager().createQuery("from " + clazz.getName())
                    .getResultList();
        }catch (NoResultException e){
            return null;
        }
    }


    @Transactional
    public T save(T entity ){
        if (entity.getId() == null) {
            getEntityManager().persist(entity);
            return entity;
        } else {
            return getEntityManager().merge(entity);
        }

    }

    public void update( T entity ){
        getEntityManager().merge( entity );
    }

    public void delete( T entity ){
        getEntityManager().remove( entity );
    }

}