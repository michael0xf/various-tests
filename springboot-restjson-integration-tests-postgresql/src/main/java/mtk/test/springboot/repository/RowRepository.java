package mtk.test.springboot.repository;

import mtk.test.springboot.model.Row;

import java.util.List;

public interface RowRepository<T extends Row> {

    T findOne(Long id);

    List<T> findAll();

    T save( T entity );

    void update(T entity);

    void delete(T entity);


}