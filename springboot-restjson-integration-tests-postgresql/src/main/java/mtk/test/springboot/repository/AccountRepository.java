package mtk.test.springboot.repository;

import mtk.test.springboot.model.Account;

public interface AccountRepository extends RowRepository<Account> {
    Account findByName(String name);
    Account findByEmail(String email);
}
