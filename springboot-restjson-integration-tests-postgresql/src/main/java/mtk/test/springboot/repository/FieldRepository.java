package mtk.test.springboot.repository;

import mtk.test.springboot.model.Field;

public interface FieldRepository extends RowRepository<Field>{
    Field findByNameAndAccountId(String name, long account_id);
}
