package mtk.test.springboot.repository;

import mtk.test.springboot.model.Field;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

@Repository
@Transactional(readOnly = true)
public class FieldRepositoryImpl extends RowRepositoryImpl<Field> implements FieldRepository{

    public FieldRepositoryImpl(){
        super();
        setClazz(Field.class);
    }


    public Field findByNameAndAccountId(String name, long account_id) {
        try {
            TypedQuery<Field> query = getEntityManager().createQuery("select a from Field a where a.name like ?1 and a.account.id = ?2", Field.class);
            query.setParameter(1, name);
            query.setParameter(2, account_id);
            return query.getSingleResult();
        }catch (NoResultException e){
            return null;
        }

    }
}
