package mtk.test.springboot.repository;

import mtk.test.springboot.model.Account;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;


@Repository
@Transactional(readOnly = true)
public class AccountRepositoryImpl extends RowRepositoryImpl<Account> implements AccountRepository{

    public AccountRepositoryImpl(){
        super();
        setClazz(Account.class);
    }

    @Override
    public Account findByName(String name) {
        try {
            TypedQuery<Account> query = getEntityManager().createQuery("select a from Account a where a.name = ?1", Account.class);
            query.setParameter(1, name);
            return query.getSingleResult();
        }catch (NoResultException e){
            return null;
        }

    }

    @Override
    public Account findByEmail(String email) {
        try{
            TypedQuery<Account> query = getEntityManager().createQuery("select a from Account a where a.email = ?1", Account.class);
            query.setParameter(1, email);
            return query.getSingleResult();
        }catch (NoResultException e){
            return null;
        }

    }

}
