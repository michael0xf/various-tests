package mtk.test.springboot;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.hibernate.internal.util.collections.CollectionHelper.isEmpty;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FieldsServicesApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FieldServicesApplicationTests {

    @LocalServerPort
    private int port;

    @Autowired
    EntityManagerFactory entityManagerFactory;

    @Transactional
    public void truncate() {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.createNativeQuery("TRUNCATE TABLE account CASCADE").executeUpdate();
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Test
    public void testAll(){
        truncate();
        ArrayList<Long> ids = putField();
        getFieldsAndGetByFieldId();
        getAccountsAndGetByAccountId();
        getFieldsTable();
        postField(ids);
        deleteField();
    }


        protected String[] getPutData(){
        return new String[]{
                "{\"Lat\": 20.9948834,\n" +
                        "\"Lon\": -49.243535,\n" +
                        "\"FieldName\": \"My field number 1\",\n" +
                        "\"AccountName\": \"User1\",\n" +
                        "\"AccountEmail\": \"user1@mail.com\"\n" +
                        "}",
                "{\"Lat\": 21.9948834,\n" +
                        "\"Lon\": -19.243535,\n" +
                        "\"FieldName\": \"My field number 2\",\n" +
                        "\"AccountName\": \"User1\",\n" +
                        "\"AccountEmail\": \"user1@mail.com\"\n" +
                        "}",
                "{\"Lat\": 20.9948834,\n" +
                        "\"Lon\": -50.243535,\n" +
                        "\"FieldName\": \"My field number 3\",\n" +
                        "\"AccountName\": \"User1\",\n" +
                        "\"AccountEmail\": \"user1@mail.com\"\n" +
                        "}",
                "{\"Lat\": 20.9948834,\n" +
                        "\"Lon\": -50.243535,\n" +
                        "\"FieldName\": \"My field number 1\",\n" +
                        "\"AccountName\": \"User2\",\n" +
                        "\"AccountEmail\": \"user2@mail.com\"\n" +
                        "}",
                "{\"Lat\": 20.9948834,\n" +
                        "\"Lon\": -50.243535,\n" +
                        "\"FieldName\": \"My field number 2\",\n" +
                        "\"AccountName\": \"User2\",\n" +
                        "\"AccountEmail\": \"user1@mail.com\"\n" +
                        "}"

        };
    }

	public ArrayList<Long> putField() {
        String[] data = getPutData();
        ArrayList<Long> ret = new ArrayList<>(data.length);
        for(int i = 0; i < data.length; i++) {
            String put = data[i];
            System.out.println("putField#" + i);
            try {
                String uri = createURLWithPort("/api/field");
                RestTemplate restTemplate = new RestTemplate();
                ResponseEntity responseEntity =
                        restTemplate.exchange(uri, HttpMethod.PUT, getPostRequestHeaders(put), String.class);
                JSONObject jsonObject = checkJSON(responseEntity.getBody().toString(), responseEntity.getHeaders());
                ret.add(jsonObject.getLong("FieldId"));
            } catch (HttpClientErrorException e) {
                System.out.println(put);
                checkJSON(e.getResponseBodyAsString(), e.getResponseHeaders());
            } catch (HttpServerErrorException e) {
                System.out.println(put);
                checkJSON(e.getResponseBodyAsString(), e.getResponseHeaders());
                assert (false);
            }catch (Exception e) {
                System.out.println(put);
                e.printStackTrace();
                assert (false);
            }
        }
        return ret;
    }

    JSONObject checkJSON(String body, HttpHeaders httpHeaders){
        System.out.println(body);

        assertEquals(MediaType.APPLICATION_JSON_UTF8, httpHeaders.getContentType());

        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(body);
            System.out.println(jsonObject.toString());
            return jsonObject;
        } catch (JSONException e) {
            e.printStackTrace();
            assert false;
        }
        return null;

    }

    JSONArray checkJSONArray(String body, HttpHeaders httpHeaders){
        System.out.println(body);

        assertEquals(MediaType.APPLICATION_JSON_UTF8, httpHeaders.getContentType());

        try {
            JSONArray jsonArray = new JSONArray(body);
            System.out.println(jsonArray.toString());
            return jsonArray;
        } catch (JSONException e) {
            e.printStackTrace();
            assert false;
        }
        return null;

    }


    HttpEntity getPostRequestHeaders(String jsonPostBody) {

        HttpHeaders reqHeaders = new HttpHeaders();
        reqHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
        reqHeaders.add("Accept", MediaType.APPLICATION_JSON_UTF8_VALUE);

        return new HttpEntity(jsonPostBody, reqHeaders);
    }
    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }


    protected JSONArray getFields(){
        try {
            String uri = createURLWithPort("/api/field");
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity responseEntity =
                    restTemplate.exchange(uri, HttpMethod.GET, new HttpEntity( new HttpHeaders()), String.class);
            return checkJSONArray(responseEntity.getBody().toString(), responseEntity.getHeaders());
        } catch (HttpClientErrorException e) {
            checkJSON(e.getResponseBodyAsString(), e.getResponseHeaders());
        } catch (HttpServerErrorException e) {
            checkJSON(e.getResponseBodyAsString(), e.getResponseHeaders());
            assert (false);
        }catch (Exception e) {
            e.printStackTrace();
            assert (false);
        }
        return null;

    }

    public void getFieldsAndGetByFieldId() {
        JSONArray fields = getFields();
        for(int i = 0; i < fields.length(); i++){
            try {
                JSONObject jsonObject = fields.getJSONObject(i);
                long id = jsonObject.getLong("FieldId");
                System.out.println("field#" + i);
                try {
                    String uri = createURLWithPort("/api/field/" + id);
                    RestTemplate restTemplate = new RestTemplate();
                    ResponseEntity responseEntity =
                            restTemplate.exchange(uri, HttpMethod.GET, new HttpEntity( new HttpHeaders()), String.class);
                    checkJSON(responseEntity.getBody().toString(), responseEntity.getHeaders());
                } catch (HttpClientErrorException e) {
                    checkJSON(e.getResponseBodyAsString(), e.getResponseHeaders());
                } catch (HttpServerErrorException e) {
                    checkJSON(e.getResponseBodyAsString(), e.getResponseHeaders());
                    assert (false);
                }catch (Exception e) {
                    e.printStackTrace();
                    assert (false);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                assert false;
            }
        }
    }

    protected JSONArray getAccounts(){
        try {
            String uri = createURLWithPort("/api/account");
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity responseEntity =
                    restTemplate.exchange(uri, HttpMethod.GET, new HttpEntity( new HttpHeaders()), String.class);
            return checkJSONArray(responseEntity.getBody().toString(), responseEntity.getHeaders());
        } catch (HttpClientErrorException e) {
            checkJSON(e.getResponseBodyAsString(), e.getResponseHeaders());
        } catch (HttpServerErrorException e) {
            checkJSON(e.getResponseBodyAsString(), e.getResponseHeaders());
            assert (false);
        }catch (Exception e) {
            e.printStackTrace();
            assert (false);
        }
        return null;
    }

    public void getAccountsAndGetByAccountId(){
        JSONArray fields = getAccounts();
        for(int i = 0; i < fields.length(); i++){
            try {
                JSONObject jsonObject = fields.getJSONObject(i);
                long id = jsonObject.getLong("AccountId");
                System.out.println("account#" + i);
                try {
                    String uri = createURLWithPort("/api/account/" + id);
                    RestTemplate restTemplate = new RestTemplate();
                    ResponseEntity responseEntity =
                            restTemplate.exchange(uri, HttpMethod.GET, new HttpEntity( new HttpHeaders()), String.class);
                    checkJSON(responseEntity.getBody().toString(), responseEntity.getHeaders());
                } catch (HttpClientErrorException e) {
                    checkJSON(e.getResponseBodyAsString(), e.getResponseHeaders());
                } catch (HttpServerErrorException e) {
                    checkJSON(e.getResponseBodyAsString(), e.getResponseHeaders());
                    assert (false);
                }catch (Exception e) {
                    e.printStackTrace();
                    assert (false);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                assert false;
            }
        }
    }

    public void getFieldsTable(){
        try {
            String uri = createURLWithPort("/");
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity responseEntity =
                    restTemplate.exchange(uri, HttpMethod.GET, new HttpEntity( new HttpHeaders()), String.class);
            System.out.println(responseEntity.getBody().toString());
        } catch (HttpClientErrorException e) {
            checkJSON(e.getResponseBodyAsString(), e.getResponseHeaders());
        } catch (HttpServerErrorException e) {
            checkJSON(e.getResponseBodyAsString(), e.getResponseHeaders());
            assert (false);
        }catch (Exception e) {
            e.printStackTrace();
            assert (false);
        }

    }


    protected String[] getPostData(){
        return new String[]{
                "{\"Lat\": 20,\n" +
                        "\"Lon\": -49,\n" +
                        "\"FieldName\": \"My field number 1\",\n" +
                        "\"AccountName\": \"User1\",\n" +
                        "\"AccountEmail\": \"user1@mail.com\"\n" +
                        "}",
                "{\"Lat\": 21.9948834,\n" +
                        "\"Lon\": -19.243535,\n" +
                        "\"FieldName\": \"My field number 2.1\",\n" +
                        "\"AccountName\": \"User1\",\n" +
                        "\"AccountEmail\": \"user1@mail.com\"\n" +
                        "}",
                "{\"Lat\": 20.9948834,\n" +
                        "\"Lon\": -50.243535,\n" +
                        "\"FieldName\": \"My field number 3.1\",\n" +
                        "\"AccountName\": \"User1\",\n" +
                        "\"AccountEmail\": \"user1@gmail.com\"\n" +
                        "}",
                "{\"Lat\": 20,\n" +
                        "\"Lon\": -50,\n" +
                        "\"FieldName\": \"My field number 1\",\n" +
                        "\"AccountName\": \"User2.2\",\n" +
                        "\"AccountEmail\": \"user2@mail.com\"\n" +
                        "}"

        };
    }

    public void postField(List<Long> ids){
        String[] data = getPostData();
        assertEquals(ids.size(), data.length);
        for(int i = 0; i < data.length; i++) {
            String put = data[i];
            System.out.println("postField#" + i);
            try {
                String uri = createURLWithPort("/api/field/" + ids.get(i));
                RestTemplate restTemplate = new RestTemplate();
                ResponseEntity responseEntity =
                        restTemplate.exchange(uri, HttpMethod.POST, getPostRequestHeaders(put), String.class);
                assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
            } catch (HttpClientErrorException e) {
                System.out.println(put);
                checkJSON(e.getResponseBodyAsString(), e.getResponseHeaders());
            } catch (HttpServerErrorException e) {
                System.out.println(put);
                checkJSON(e.getResponseBodyAsString(), e.getResponseHeaders());
                assert (false);
            }catch (Exception e) {
                System.out.println(put);
                e.printStackTrace();
                assert (false);
            }
        }
    }

    public void deleteField(){
        JSONArray fields = getFields();
        for(int i = 0; i < fields.length(); i++){
            try {
                JSONObject jsonObject = fields.getJSONObject(i);
                String name = jsonObject.getString("AccountName");
                if (name.equals("User2.2")) {
                    try {
                        long id = jsonObject.getLong("FieldId");
                        System.out.println("delete field#" + i);
                        String uri = createURLWithPort("/api/field/" + id);
                        RestTemplate restTemplate = new RestTemplate();
                        ResponseEntity responseEntity =
                                restTemplate.exchange(uri, HttpMethod.DELETE, new HttpEntity(new HttpHeaders()), String.class);
                        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
                    } catch (HttpClientErrorException e) {
                        checkJSON(e.getResponseBodyAsString(), e.getResponseHeaders());
                    } catch (HttpServerErrorException e) {
                        checkJSON(e.getResponseBodyAsString(), e.getResponseHeaders());
                        assert (false);
                    } catch (Exception e) {
                        e.printStackTrace();
                        assert (false);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
                assert false;
            }
        }
    }


}
