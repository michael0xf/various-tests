package test.entry.xml.impl;

import test.Log;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

/**
 * Created with IntelliJ IDEA.
 * User: MKRAFT
 * Date: 9/21/13
 * Time: 11:22 AM
 * To change this template use File | Settings | File Templates.
 */
public class Transformator {
    public static void transform(String template, String doc1, String doc2) throws TransformerException {
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer(new StreamSource(template));
        transformer.transform(new StreamSource(doc1), new StreamResult(doc2));
        Log.out("transform complete");
    }
}
