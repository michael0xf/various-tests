package test.entry.xml.impl;

import test.ExternalNames;
import test.Log;
import test.entry.xml.XMLElement;
import test.entry.xml.XMLException;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * Created with IntelliJ IDEA.
 * User: MKRAFT
 * Date: 9/21/13
 * Time: 11:16 AM
 * To change this template use File | Settings | File Templates.
 */
public class Entries implements XMLElement {
    int total = 0;
    final Entry entry = new Entry();

    class Entry implements XMLElement {
        public String getName() {
            return ExternalNames.ENTRY;
        }

        public XMLElement startElement(XMLStreamReader reader) {
            return null;  //internal elements not supported
        }

        public void end() throws XMLException {

        }
    }

    public void end() throws XMLException {
        Log.out("Calculate complete. Total = " + total);
    }

    public String getName() {
        return ExternalNames.ENTRIES;
    }

    public XMLElement startElement(XMLStreamReader reader) throws XMLStreamException, XMLException {
        String name = reader.getLocalName();
        if (!name.equals(entry.getName()))
            return null;  //other elements not supported
        int count = reader.getAttributeCount();
        if (count != 1)
            throw new XMLException("Unsupported attributes count");
        final String aname = reader.getAttributeName(0).toString();
        if (!ExternalNames.XMLFIELD.equals(aname)) {
            throw new XMLException("Unsupported attribute: " + reader.getAttributeName(0));
        }
        String value = reader.getAttributeValue(0);
        total += Integer.parseInt(value);
        return entry;
    }
}
