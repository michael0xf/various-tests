package test.entry.xml.impl;

import test.entry.xml.XMLElement;
import test.entry.xml.XMLException;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.stream.StreamSource;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: MKRAFT
 * Date: 9/21/13
 * Time: 11:19 AM
 * To change this template use File | Settings | File Templates.
 */
public class Calculator {
    public static void calculate(String fname) throws XMLException, XMLStreamException {
        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = inputFactory.createXMLStreamReader(new StreamSource(fname));
        final RootElement rootElement = new RootElement();
        ArrayList<XMLElement> stack = new ArrayList<XMLElement>();
        XMLElement current = rootElement;
        br:
        while (reader.hasNext()) {
            if (current == null) {
                throw new XMLException("Unsupported XML");
            }
            int type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    stack.add(current);
                    current = current.startElement(reader);
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    String name = reader.getLocalName();
                    if (current.getName().equals(name)) {
                        current.end();
                    } else {
                        throw new XMLException("XML error");
                    }
                    int last = stack.size() - 1;
                    if (last < 0)
                        throw new XMLException("Unknown exception");
                    current = stack.get(last);
                    stack.remove(last);
            }

        }
        reader.close();
    }
}
