package test.entry.xml.impl;

import test.entry.xml.XMLElement;
import test.entry.xml.XMLException;

import javax.xml.stream.XMLStreamReader;

/**
 * Created with IntelliJ IDEA.
 * User: MKRAFT
 * Date: 9/21/13
 * Time: 11:17 AM
 * To change this template use File | Settings | File Templates.
 */
public final class RootElement implements XMLElement {
    public final Entries ENTRIES = new Entries();

    public XMLElement startElement(XMLStreamReader reader) {
        String name = reader.getLocalName();
        if (name.equals(ENTRIES.getName()))
            return ENTRIES;
        else
            return null;  //no supporting other elements
    }

    public String getName() throws XMLException {
        throw new XMLException("XML error");
    }

    public void end() throws XMLException {
        throw new XMLException("XML error");
    }
}
