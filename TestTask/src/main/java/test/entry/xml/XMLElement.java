package test.entry.xml;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
* Created with IntelliJ IDEA.
* User: MKRAFT
* Date: 9/21/13
* Time: 11:16 AM
* To change this template use File | Settings | File Templates.
*/
public interface XMLElement
{
    public String getName() throws XMLException;
    /*
       @return expect XMLElement
    */
    public XMLElement startElement(XMLStreamReader reader)  throws XMLStreamException, XMLException;
    public void end() throws XMLException;

}
