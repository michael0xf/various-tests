package test.entry.xml;

/**
* Created with IntelliJ IDEA.
* User: MKRAFT
* Date: 9/21/13
* Time: 11:15 AM
* To change this template use File | Settings | File Templates.
*/
public class XMLException extends Exception
{
    public XMLException(String message)
    {
        super(message);
    }
}
