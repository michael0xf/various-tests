package test.entry.dao;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;

public interface TestDAO
{
	public void insertFields(int number) throws SQLException, IOException;
	public void clearTable()  throws SQLException;

    public void writeXMLFileFromDB(FileWriter wrt) throws SQLException, IOException;
    public void createTable()  throws SQLException;
	

}




