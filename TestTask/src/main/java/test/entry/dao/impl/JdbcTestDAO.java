package test.entry.dao.impl;

import org.postgresql.copy.CopyManager;
import org.postgresql.core.BaseConnection;
import test.ExternalNames;
import test.Log;
import test.entry.dao.TestDAO;

import javax.sql.DataSource;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class JdbcTestDAO implements TestDAO, ExternalNames {
    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }


    @Override
    public void createTable() throws SQLException {
        final String sql = "CREATE TABLE " + TABLE + "( " + FIELD + " int )";
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            final Statement s = conn.createStatement();
            s.execute(sql);
            s.close();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    Log.out(e);
                }
            }
        }
    }

    @Override
    public void clearTable() throws SQLException {
        final String sql = "TRUNCATE TABLE " + TABLE;
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            final Statement s = conn.createStatement();
            s.execute(sql);
            s.close();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    Log.out(e);
                }
            }
        }
    }

    @Override
    public void insertFields(final int number) throws SQLException, IOException {
        clearTable();
        if (number < 1)
            return;
        final String sql = "COPY " + TABLE + " (" + FIELD + ") FROM STDIN";
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            CopyManager copyManager = new CopyManager((BaseConnection) conn);

            InputStream inputStream = new InputStream() {
                final StringBuilder stdin = new StringBuilder();
                int i = 0;
                int pos = 0;

                @Override
                public int read() throws IOException {
                    if (stdin.length() - pos == 0) {
                        i++;
                        if (pos > 0)
                            stdin.delete(0, pos);
                        pos = 0;
                        if (i > number)
                            return -1;
                        stdin.append(i);
                        if (i < number)
                            stdin.append('\n');
                    }
                    try {
                        try {
                            return stdin.charAt(pos);
                        } catch (Exception e) {
                            Log.out(e);
                            return -1;
                        }

                    } finally {
                        pos++;
                    }
                }

                public int available() {
                    if (stdin.length() - pos > 0)
                        return 1;
                    else if (number - i > 0)
                        return 1;
                    else
                        return 0;
                }
            };
            copyManager.copyIn(sql, inputStream);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    Log.out(e);
                }
            }
        }
    }

    @Override
    public void writeXMLFileFromDB(final FileWriter wrt) throws SQLException, IOException {
        final String sql = "COPY (SELECT * FROM " + TABLE + ") TO STDOUT";
        Connection conn = null;

        try {
            conn = dataSource.getConnection();
            CopyManager copyManager = new CopyManager((BaseConnection) conn);
            //XMLOutputFactory was slow, sql fastest
            final String indent0 = "    ";
            final String indent1 = indent0 + indent0;
            final String s0 = indent0 + "<" + ENTRY + ">\n" +
                    indent1 + "<" + XMLFIELD + ">";
            final String s1 = "</" + XMLFIELD + ">\n" +
                    indent0 + "</" + ENTRY + ">\n";
            OutputStream out = new OutputStream() {
                boolean isEntry = false;
                int counter = 0;

                @Override
                public void write(final int b) throws IOException {
                    if (!isEntry) {
                        wrt.write(s0);
                        counter += s0.length();
                        isEntry = true;
                    }
                    if (b == '\n') {

                        wrt.write(s1);
                        counter += s1.length();
                        ;
                        isEntry = false;

                    } else {
                        wrt.write((char) b);
                        counter++;
                    }
                    if (counter > 32700)//a bit less then hypothetical block
                    {
                        wrt.flush();
                        counter = 0;
                    }
                }
            };
            wrt.append("<" + ENTRIES + ">\n");
            copyManager.copyOut(sql, out);
            wrt.append("</" + ENTRIES + ">\n");
            wrt.flush();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    Log.out(e);
                }
            }
        }
    }


}

