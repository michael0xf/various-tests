package test;

public class Log {

    public static void out(Throwable e) {
        e.printStackTrace(System.out);
        System.out.println(e.getMessage());
    }

    public static void out(String message) {
        System.out.println(message);
    }

}
