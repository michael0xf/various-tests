package test.common;

import test.ExternalNames;
import test.Log;
import test.entry.dao.TestDAO;
import test.entry.dao.impl.JdbcTestDAO;
import test.entry.xml.impl.Calculator;
import test.entry.xml.impl.Transformator;

import javax.sql.DataSource;
import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.Properties;
import java.util.logging.Logger;

public class App implements ExternalNames
{
    // Приложение сперва разработал на EE, потом портировал на SE заменив имплементации спринга, но не трогая само приложение,
    // т.е., например, java bean JdbcTestDAO не менялся.

    public static class ContextJDE6
    {
        private String propertiesPath;
        private DataSource dataSource;//Интерфейс, используемый в спринге, мы его также используем
        private JdbcTestDAO jdbcTestDAO;

        public void clearFields()
        {
            propertiesPath = null;
            dataSource = null;
            jdbcTestDAO = null;
        }

        public void setPropertiesPath(String propertiesPath) {
            clearFields();//если мы извне изменили параметры подключения, то остальные поля уже не определёны
            this.propertiesPath = propertiesPath;
        }

        public void setDataSource(DataSource dataSource) {
            clearFields();//если мы извне изменили dataSource, то остальные поля уже не определёны
            this.dataSource = dataSource;
        }

        public DataSource getDataSource()
        {
            if (dataSource != null)
                return dataSource;
            try{
                Properties properties = new Properties();
                properties.load(new FileInputStream(propertiesPath));
                final String driverClassName = properties.getProperty(DRIVER);
                final String url = properties.getProperty(URL);
                final String username = properties.getProperty(USER);
                final String password = properties.getProperty(PSWRD);

                dataSource = new DataSource() {
                    @Override
                    public Connection getConnection() throws SQLException {
                        try {

                            Class.forName(driverClassName);

                        } catch (ClassNotFoundException e) {

                            Log.out("Where is your JDBC Driver? Include in your library path!");
                            Log.out(e);
                            return null;

                        }

                        Log.out("PostgreSQL JDBC Driver Registered.");

                        Connection connection = DriverManager.getConnection(url , username,password);
                        if (connection != null) {
                            Log.out("You made it, take control your database now.");
                        } else {
                            Log.out("Failed to make connection.");
                        }
                        return connection;
                    }

                    @Override
                    public Connection getConnection(String username, String password) throws SQLException {
                        return null;  //To change body of implemented methods use File | Settings | File Templates.
                    }

                    @Override
                    public PrintWriter getLogWriter() throws SQLException {
                        return null;  //To change body of implemented methods use File | Settings | File Templates.
                    }

                    @Override
                    public void setLogWriter(PrintWriter out) throws SQLException {
                        //To change body of implemented methods use File | Settings | File Templates.
                    }

                    @Override
                    public void setLoginTimeout(int seconds) throws SQLException {
                        //To change body of implemented methods use File | Settings | File Templates.
                    }

                    @Override
                    public int getLoginTimeout() throws SQLException {
                        return 0;  //To change body of implemented methods use File | Settings | File Templates.
                    }

                    @Override
                    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
                        return null;  //To change body of implemented methods use File | Settings | File Templates.
                    }

                    @Override
                    public <T> T unwrap(Class<T> iface) throws SQLException {
                        return null;  //To change body of implemented methods use File | Settings | File Templates.
                    }

                    @Override
                    public boolean isWrapperFor(Class<?> iface) throws SQLException {
                        return false;  //To change body of implemented methods use File | Settings | File Templates.
                    }
                };
            }   catch(IOException e)
            {
                Log.out(e);
            }
            return dataSource;
        }

        public TestDAO getTestDAO()
        {
            if (jdbcTestDAO != null)
                return jdbcTestDAO;
            JdbcTestDAO jdbcTestDAO = new JdbcTestDAO();
            jdbcTestDAO.setDataSource(getDataSource());
            this.jdbcTestDAO = jdbcTestDAO;
            return jdbcTestDAO;
        }
    }




    public static void main( String[] args )
    {
    	try
    	{
            //Спрингом было бы так:
            //ApplicationContext context = new ClassPathXmlApplicationContext("Spring-Module.xml");
            //TestDAO testDAO = (TestDAO) context.getBean("TestDAO");

            String dbProperties = PROPERTIES;
            int number = 10;//миллион за 11 секунд на ноутбуке + pgsql
            if ((args != null)&&(args.length > 0))
            {
                if (args.length == 2)
                {
                    dbProperties = args[0];
                    number = Integer.parseInt(args[1]);
                }else if ((args.length == 1)&&(args[0].equalsIgnoreCase("create")))
                {
                    ContextJDE6 context = new ContextJDE6();
                    context.setPropertiesPath(dbProperties);
                    TestDAO testDAO = context.getTestDAO();
                    testDAO.createTable();
                    return;
                }else
                {
                    Log.out("Invalid number of arguments");
                    return;
                }
            }

            //Эмулируем спринг в меру сил:
            ContextJDE6 context = new ContextJDE6();
            context.setPropertiesPath(dbProperties);
            TestDAO testDAO = context.getTestDAO();
            //миллион за две секунды
            testDAO.insertFields(number);
            File flt = new File(DOC1);
            FileWriter wrt = new FileWriter(flt);
            //миллион за секунду
            //Поскольку требуется скорость выполнения, XMLOutputFactory лишний, sql его сильно обогнал
            wrt.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
            testDAO.writeXMLFileFromDB(wrt);
            wrt.close();
            //трансформирует за пять секунд
            Transformator.transform(TEMPLATE, DOC1, DOC2);
            //парсим за три секунды с помощью XMLInputFactory
            Calculator.calculate(DOC2);



            /*StringBuffer data =
            String s = data.toString();
            Log.out(s); */
    	}catch(Throwable e)
    	{
    		Log.out(e);
    	} finally
        {

        }
        
    }
}
