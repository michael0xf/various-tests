package test;

/**
 * Created with IntelliJ IDEA.
 * User: MKRAFT
 * Date: 9/20/13
 * Time: 8:47 AM
 * To change this template use File | Settings | File Templates.
 */
public interface ExternalNames {
    //file names:
    String DOC1 = "1.xml";
    String DOC2 = "2.xml";
    String TEMPLATE = "template.xsl";
    String PROPERTIES = "dataSource.properties";
    //in properties:
    String DRIVER = "driverClassName";
    String URL = "jdbcUrl";
    String USER = "username";
    String PSWRD = "password";
    //in DB:
    String TABLE = "TEST";
    String FIELD = "FIELD";
    //XML & XSL:
    String ENTRIES = "entries";
    String ENTRY = "entry";
    String XMLFIELD = "field";


}
