���� ������� phones � ������:
phone - varchar
users - int[]

���� ������ ������� items
id serial
user_id int
status smallint (3 - �� ������, 7 - ������, 5 - ������)

1. ���� �������� ������ ������� �� �������� �������� ���������� ���������� ��������� �����. ����� ����
----------------
7924445544 | 5
8985545444 | 0

�����:

WITH
t1 AS (
	SELECT phone, status 
	FROM phones, items 
	WHERE (((phone = '7924445544') OR (phone = '8985545444')) AND (user_id = ANY (users)))
),
t2 AS(
	SELECT phone, COUNT(phone)
	FROM t1
	WHERE status=7
	GROUP BY phone
)
SELECT *
FROM t2
UNION 
SELECT phone, 0
FROM t1
WHERE phone NOT IN ( SELECT phone FROM t2 )
;

2) ������� ���������� � ����� ������� ���������� � ��������� � �����������

�����:


WITH
t AS (
	SELECT phone, status 
	FROM phones, items 
	WHERE (((phone = '7924445544') OR (phone = '8985545444') OR (phone = '12345') OR (phone = '777')) AND (user_id = ANY (users)))
),
t7 AS(
	SELECT phone, COUNT(phone) as count7
	FROM t
	WHERE t.status = 7
	GROUP BY phone
),
t07 AS(
	SELECT phone, count7
	FROM t7
	UNION
	SELECT phone, 0
	FROM t
	WHERE phone NOT IN ( SELECT phone FROM t7 )),
t35 AS(
	SELECT phone, COUNT(phone) as count35
	FROM t
	WHERE t.status <> 7
	GROUP BY phone
),
t035 AS(
	SELECT phone, count35
	FROM t35
	UNION
	SELECT phone, 0
	FROM t
	WHERE phone NOT IN ( SELECT phone FROM t35 )
)
SELECT t07.phone, count7, count35
FROM t07 JOIN t035 ON t07.phone=t035.phone
;
