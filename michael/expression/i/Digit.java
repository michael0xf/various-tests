package michael.expression.i;

public class Digit extends HighPriorityExpression {
    public Digit(int p_value) {
        value = p_value;
    }

    public int getValue() {
        return value;
    }

    public void accept(Visitor v) {
        v.visitDigit(this);
    }

    private final int value;
}
