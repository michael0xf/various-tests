package michael.expression.i;

public interface Expression {
    void accept(Visitor v);
}
