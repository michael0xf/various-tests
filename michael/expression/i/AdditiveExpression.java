package michael.expression.i;

public class AdditiveExpression extends AdditivePriorityExpression {
    public AdditiveExpression(MultiplicativePriorityExpression[] p_terms) {
        terms = p_terms;
    }

    public int sizeTerms() {
        return terms.length;
    }

    public MultiplicativePriorityExpression getTerm(int n) {
        return terms[n];
    }

    public void accept(Visitor v) {
        v.visitAdditiveExpression(this);
    }

    private final MultiplicativePriorityExpression[] terms;
}
