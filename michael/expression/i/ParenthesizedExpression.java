package michael.expression.i;

public class ParenthesizedExpression extends HighPriorityExpression {
    public ParenthesizedExpression(AdditivePriorityExpression p_expr) {
        expr = p_expr;
    }

    public AdditivePriorityExpression getExpression() {
        return expr;
    }

    public void accept(Visitor v) {
        v.visitParenthesizedExpression(this);
    }


    private final AdditivePriorityExpression expr;
}
