package michael.expression.i;

public class MultiplicativeExpression extends MultiplicativePriorityExpression {
    public MultiplicativeExpression(HighPriorityExpression[] p_factors) {
        factors = p_factors;
    }

    public int sizeFactors() {
        return factors.length;
    }

    public HighPriorityExpression getFactor(int n) {
        return factors[n];
    }

    public void accept(Visitor v) {
        v.visitMultiplicativeExpression(this);
    }

    private final HighPriorityExpression[] factors;
}
