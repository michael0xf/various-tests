package michael.expression.i;

public interface Visitor {
    void visitAdditiveExpression(AdditiveExpression node);

    void visitMultiplicativeExpression(MultiplicativeExpression node);

    void visitParenthesizedExpression(ParenthesizedExpression node);

    void visitDigit(Digit node);
}
