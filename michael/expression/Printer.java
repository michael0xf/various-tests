package michael.expression;

import michael.expression.i.*;

public class Printer implements Visitor {

    StringBuffer exp_s = new StringBuffer("");

    public StringBuffer ExpToStr(Expression exp) {
        exp.accept(this);
        return exp_s;
    }

    public void visitAdditiveExpression(AdditiveExpression node) {
        for (int i = 0; i < node.sizeTerms(); i++) {
            node.getTerm(i).accept(this);
            if (i < (node.sizeTerms() - 1))
                exp_s.append('+');
        }
    }

    public void visitMultiplicativeExpression(MultiplicativeExpression node) {
        for (int i = 0; i < node.sizeFactors(); i++) {
            node.getFactor(i).accept(this);
            if (i < (node.sizeFactors() - 1))
                exp_s.append('*');
        }
    }

    public void visitParenthesizedExpression(ParenthesizedExpression node) {
        exp_s.append('(');
        node.getExpression().accept(this);
        exp_s.append(')');
    }

    public void visitDigit(Digit node) {
        exp_s.append((char) (node.getValue() + 48));
    }
}


