package michael.expression;

import michael.expression.i.*;

public class PrintTree {

    public static void main(String args[]) {
        Expression exp;
        Parse par = new Parse();
        printTree(par.parse(args[0]));

    }

    public static void printTree(Expression exp) {
        Dumper d = new Dumper();
        Printer p = new Printer();
        Calculator c = new Calculator();

        if (exp == null) {
            System.out.println("null");
        } else {
            d.Tree(exp);
            System.out.println();
            System.out.println(p.ExpToStr(exp));
            System.out.println();
            System.out.println(c.calculation(exp));
        }
        System.out.println();
    }
}