package michael.expression;

import michael.expression.i.*;

public class Dumper implements Visitor {
    StringBuffer shift = new StringBuffer("");

    public void Tree(Expression exp) {

        exp.accept(this);
    }

    public void visitAdditiveExpression(AdditiveExpression node) {

        System.out.println(shift + "Additive");
        shift.append(" ");
        for (int i = 0; i < node.sizeTerms(); i++) {
            node.getTerm(i).accept(this);
        }
        shift.deleteCharAt(shift.length() - 1);
    }

    public void visitMultiplicativeExpression(MultiplicativeExpression node) {
        System.out.println(shift + "Multiplicative");
        shift.append(" ");
        for (int i = 0; i < node.sizeFactors(); i++) {
            node.getFactor(i).accept(this);
        }
        shift.deleteCharAt(shift.length() - 1);
    }

    public void visitParenthesizedExpression(ParenthesizedExpression node) {
        System.out.println(shift + "Parenthesized");
        shift.append(" ");
        node.getExpression().accept(this);
        shift.deleteCharAt(shift.length() - 1);
    }

    public void visitDigit(Digit node) {
        System.out.println(shift + "Digit = " + node.getValue());
    }
}


