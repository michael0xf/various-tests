package michael.expression;

import michael.expression.i.*;

import java.util.Vector;

public class Parse {

    public Expression parse(String text) {
        try {
            return toTree(text);
        } catch (ParseException e) {
            return null;
        }
    }


    public Expression toTree(String text) throws ParseException

    {

        Expression exp = null;
        Expression elements[];

        text = text.trim();
        if (text.length() > 0) {
            elements = (MultiplicativePriorityExpression[]) operation(text, '+').toArray(new MultiplicativePriorityExpression[0]);
            if (elements.length != 0) {
                exp = new AdditiveExpression((MultiplicativePriorityExpression[]) elements);
            } else {
                elements = (HighPriorityExpression[]) operation(text, '*').toArray(new HighPriorityExpression[0]);
                if (elements.length != 0) {
                    exp = new MultiplicativeExpression((HighPriorityExpression[]) elements);
                } else {
                    char c0 = text.charAt(0);
                    int len = text.length();
                    if ((c0 == '(') && (text.charAt(len - 1) == ')')) {
                        String str = new String();
                        str = text.substring(1, len - 1);
                        exp = toTree(str);
                        if (exp != null) {
                            exp = new ParenthesizedExpression((AdditivePriorityExpression) exp);
                        }
                    } else if ((len == 1) && (c0 >= '0') && (c0 <= '9')) {
                        exp = new Digit((int) c0 - 48);
                    }
                }
            }
        }
        return exp;
    }


    public Vector operation(String text, char o) throws ParseException {
        int len = text.length();
        if (text.charAt(len - 1) == o) throw new ParseException();
        Vector vec = new Vector();
        int par = 0;
        int amount = 0;
        char c;
        StringBuffer str = new StringBuffer("");
        for (int i = 0; i < len; i++) {
            c = text.charAt(i);
            if (c == '(') par++;
            if (c == ')') par--;
            if (((c >= '0') & (c <= '9')) || (c == '+') || (c == '*') || (c == '(') || (c == ')')) {
                if (!((c == o) && (par == 0))) {
                    str.append(c);
                }
                if ((par == 0) && ((c == o) ^ ((i == (len - 1)) && (amount > 0)))) {
                    Expression exp = toTree(str.toString());
                    if (exp != null) {
                        vec.add(exp);
                        amount++;
                        str.delete(0, str.length());
                    } else throw new ParseException();
                }
            } else if (c != ' ') throw new ParseException();
        }
        if (par != 0) throw new ParseException();
        return vec;
    }

}

class ParseException extends Exception {
}

