package michael.expression;

import michael.expression.i.*;


public class Calculator implements Visitor {
    long num;

    public long calculation(Expression exp) {
        exp.accept(this);
        return num;
    }

    public void visitAdditiveExpression(AdditiveExpression node) {
        long n = 0;
        for (int i = 0; i < node.sizeTerms(); i++) {
            node.getTerm(i).accept(this);
            n += num;
            num = n;
        }
    }

    public void visitMultiplicativeExpression(MultiplicativeExpression node) {
        long n = 1;
        for (int i = 0; i < node.sizeFactors(); i++) {
            node.getFactor(i).accept(this);
            n *= num;
            num = n;
        }

    }

    public void visitParenthesizedExpression(ParenthesizedExpression node) {
        node.getExpression().accept(this);
    }

    public void visitDigit(Digit node) {
        num = node.getValue();
    }
}


