/**
 * Created with IntelliJ IDEA.
 * User: MKRAFT
 * Date: 1/21/15
 * Time: 7:15 PM
 * To change this template use File | Settings | File Templates.
 */
/*
 * Sorry, javadocs empty.
 */
public class FibCalcImpl implements FibCalc {
    public long fib(int n){
        if (n <= 0) return 0L;
        else if(n == 1) return 1L;
        else {
            long value0 = 0L;
            long value1 = 1L;
            long value2 = 1L;
            for (int i = 0; i < n - 1; i++){
                value2 = value0 + value1;
                value0 = value1;
                value1 = value2;
            }
            return value2;
        }
    }
}
