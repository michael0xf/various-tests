import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: MKRAFT
 * Date: 1/21/15
 * Time: 7:25 PM
 * To change this template use File | Settings | File Templates.
 */
/*
 * Sorry, javadocs empty.
 */
public class FibCalcImplTest {


    public FibCalcImplTest(final int n, final int calculationCount, final int threadPoolSize){
        System.out.println("Argument: " + n);
        System.out.println("CalculationCount: " + calculationCount);
        System.out.println("ThreadPoolSize: " + threadPoolSize);
        try {
            final ArrayList<Long> results = new ArrayList<Long>(calculationCount);
            final PerfomanceTesterImpl perfomanceTesterImpl = new PerfomanceTesterImpl();
            final PerformanceTestResult performanceTestResult = perfomanceTesterImpl.runPerformanceTest( new Runnable() {
                @Override
                public void run() {
                    results.add(new FibCalcImpl().fib(n));
                }
            }, calculationCount, threadPoolSize);
            if (results.size() != calculationCount){
                throw new InterruptedException("Unknown error: results.size() != calculationCount");
            }
            final long result = results.get(0);
            System.out.println("Result: " + result);
            for(long next:results){
                if (next != result){
                    System.out.println("Unknown error: difference in the results: " + result +" and " + next);
                }
            }
            System.out.println("Min time: " + performanceTestResult.getMinTime());
            System.out.println("Max time: " + performanceTestResult.getMaxTime());
            System.out.println("Total time: " + performanceTestResult.getTotalTime());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(final String[] args){
        try{
            new FibCalcImplTest(Integer.parseInt(args[0]), Integer.parseInt(args[1]), Integer.parseInt(args[2]));
        }catch (Exception e){
            System.out.println(e.getMessage());
            System.out.println("Bad arguments? Has to be three integers: n, calculationCount, threadPoolSize.");
        }
    }
}
