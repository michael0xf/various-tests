/**
 * Created with IntelliJ IDEA.
 * User: MKRAFT
 * Date: 1/21/15
 * Time: 7:18 PM
 * To change this template use File | Settings | File Templates.
 */

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/*
 * Sorry, javadocs empty.
 */
public class PerfomanceTesterImpl implements PerformanceTester{

    int counter = 0;
    long maxTime = 0;
    long minTime = Long.MAX_VALUE;
    final long MAXIMUM_MS_LIMIT = TimeUnit.DAYS.toMillis(1);

    synchronized int getNumber(){
        this.counter++;
        return this.counter;
    }

    synchronized void newTime(final long time){
        if (this.maxTime < time){
            this.maxTime = time;
        }else if (this.minTime > time){
            this.minTime = time;
        }
    }

    protected void fillIn(final ExecutorService service, final ArrayList<Future> futures, final Runnable task, final long executionCount){
        for (int i = 0; i < executionCount; i++){
            futures.add(service.submit(
                new Runnable() {
                    @Override
                    public void run() {
                        final long startTime = System.currentTimeMillis();
                        getNumber();
                        task.run();
                        final long endTime = System.currentTimeMillis();
                        newTime(endTime - startTime);
                    }
                }
            ));
        }
    }

    protected void doMonitor(final ExecutorService service, final ArrayList<Future> futures, final long startTime){
        while((futures.size() > 0) && ( !service.isTerminated() ) && ( !service.isShutdown() )){
            if (System.currentTimeMillis() - startTime > MAXIMUM_MS_LIMIT){
                new InterruptedException("Error: endTime - startTime > " + MAXIMUM_MS_LIMIT);
            }
            final Iterator<Future> it = futures.iterator();
            while(it.hasNext()){
                final Future future = it.next();
                if (future.isDone()){
                    it.remove();
                }
            }
            Thread.yield();
        }
    }

    @Override
    public PerformanceTestResult runPerformanceTest(final Runnable task, final int executionCount, final int threadPoolSize) throws InterruptedException {
        final long startTime = System.currentTimeMillis();
        final ExecutorService service = Executors.newFixedThreadPool(threadPoolSize);
        final ArrayList<Future> futures = new ArrayList<Future>(executionCount);
        fillIn(service, futures, task, executionCount);
        doMonitor(service, futures, startTime);
        service.shutdown();
        if (futures.size() > 0){
            throw new InterruptedException("Unknown error: futures.size() > 0");
        }
        if (this.minTime > this.maxTime){
            throw new InterruptedException("Unknown error: this.minTime > this.maxTime");
        }
        System.out.println(counter + " tasks performed.");
        return new PerformanceTestResult(System.currentTimeMillis() - startTime, this.minTime, this.maxTime);
    }
}
