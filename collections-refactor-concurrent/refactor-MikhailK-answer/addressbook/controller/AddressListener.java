package addressbook.controller;

import addressbook.model.Person;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: MKRAFT
 * Date: 1/22/15
 * Time: 9:59 AM
 * To change this template use File | Settings | File Templates.
 */
/*
 * Sorry, javadocs empty.
 */
public interface AddressListener {
    public void event(List<Person> list);
}
