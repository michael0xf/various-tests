package addressbook.controller;
import addressbook.model.AddressBook;
import addressbook.model.db.impl.AddressDb;

/**
* Created with IntelliJ IDEA.
* User: MKRAFT
* Date: 1/22/15
* Time: 9:48 AM
* To change this template use File | Settings | File Templates.
*/
/*
 * Sorry, javadocs empty.
 */
public class Checker implements Runnable {

    AddressDb addressDb;
    AddressListener addressListener;
    boolean isAlive;

    public Checker(AddressDb addressDb, AddressListener addressListener){
        this.addressListener = addressListener;
        this.addressDb = addressDb;
    }

    public void stop(){
        isAlive = false;
    }

    public void start(){
        new Thread(this).start();
    }


    public void run() {
        isAlive = true;
        AddressBook addressBook = new AddressBook(addressDb);
        while(isAlive) {
            addressListener.event(addressBook.getPersonsHavingMobilePhone());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
        }
    }
}
