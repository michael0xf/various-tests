package addressbook.model.db.impl;

import addressbook.model.MultipleIdenticalRecordsException;
import addressbook.model.Person;
import addressbook.model.PhoneNumber;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;
/*
 * Sorry, javadocs empty.
 */
abstract public class AddressDb {

    private String password;
    private String login;
    private String url;
    private String driver;
    public final String NAME_COL = "name";
    public final String PHONE_NUMBER_COL = "phoneNumber";
    public final String ADDRESS_ENTRY = "AddressEntry";


    public AddressDb(String driver, String url, String login, String password) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        this.driver = driver;
        this.url = url;
        this.login = login;
        this.password = password;
        Class.forName(driver).newInstance();
    }

    Connection openConnection() throws SQLException {
        return DriverManager.getConnection(url, login, password);
    }

    public void addPerson(final Person person) throws SQLException {
        final Connection connection = openConnection();
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement("insert into " + ADDRESS_ENTRY + " values (?, ?, ?)");
            statement.setLong(1, System.currentTimeMillis());
            statement.setString(2, person.getName());
            statement.setString(3, person.getPhoneNumber().getNumber());
            statement.executeUpdate();
        } finally {
            close(connection, statement, null);
        }
    }

    /**
     * Looks up the given person, null if not found.
     */
    public Person findPerson(final String name) throws SQLException, IllegalArgumentException, MultipleIdenticalRecordsException {
        final Connection connection = openConnection();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement("select * from " + ADDRESS_ENTRY + " where " + NAME_COL + " = '" + name + "'");
            if (statement == null)
                return null;
            resultSet = statement.executeQuery();
            if (resultSet == null)
                return null;
            if (resultSet.next()) {
                if (resultSet.next())
                    throw new MultipleIdenticalRecordsException(name);
                return newPerson(resultSet);
            }
            return null;
        } finally {
            close(connection, statement, resultSet);
        }
    }

    /**
     * Get all persons, empty if not found.
     */
    public List<Person> getAll() throws SQLException {
        final Connection connection = openConnection();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        final List<Person> entries = new LinkedList<Person>();
        try {
            statement = connection.prepareStatement("select * from " + ADDRESS_ENTRY);
            if (statement == null)
                return entries;
            resultSet = statement.executeQuery();
            if (resultSet == null)
                return entries;
            while (resultSet.next()) {
                entries.add(newPerson(resultSet));
            }
            return entries;
        } finally {
            close(connection, statement, resultSet);
        }
    }

    Person newPerson(ResultSet resultSet) throws SQLException {
        final String name = resultSet.getString(NAME_COL);
        final String number = resultSet.getString(PHONE_NUMBER_COL);
        final PhoneNumber phoneNumber = new PhoneNumber(number, isMobile(number));
        return new Person(name, phoneNumber);
    }


    boolean isMobile(final String number) {
        return number.startsWith("070");//?
    }

   /*
    * This is abstract class. Please should implement method "getSize" which optimized for your database.
    */
    public int getSize() throws SQLException {
        return getAll().size();
    }


    void close(Connection connection, Statement statement, ResultSet resultSet) {
        try {
            if (resultSet != null)
                resultSet.close();
        } catch (SQLException err) {
            throw new RuntimeException(err);
        }
        try {
            if (statement != null)
                statement.close();
        } catch (SQLException err) {
            throw new RuntimeException(err);
        }
        try {
            if (connection != null)
                connection.close();
        } catch (SQLException err) {
            throw new RuntimeException(err);
        }
    }
}