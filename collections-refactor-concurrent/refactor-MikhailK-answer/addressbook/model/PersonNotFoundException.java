package addressbook.model;

/**
 * Created with IntelliJ IDEA.
 * User: MKRAFT
 * Date: 1/22/15
 * Time: 9:10 AM
 * To change this template use File | Settings | File Templates.
 */
/*
 * Sorry, javadocs empty.
 */
public class PersonNotFoundException extends Exception {
    public PersonNotFoundException(final String name) {
        super(name);
    }

}
