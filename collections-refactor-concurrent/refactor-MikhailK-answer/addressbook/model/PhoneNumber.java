package addressbook.model;
/*
 * Sorry, javadocs empty.
 */
public class PhoneNumber {
    private Num number;
    private boolean isMobile;

    public PhoneNumber(final String number, final boolean isMobile) {
        this.number = new Num(number);
        this.isMobile = isMobile;
    }

    public String getNumber() {
        return number.getNumber();
    }

    public boolean isMobile() {
        return isMobile;
    }


}
