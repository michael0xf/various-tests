package addressbook.model;

import addressbook.model.db.impl.AddressDb;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
/*
 * Sorry, javadocs empty.
 */
public class AddressBook {
    final AddressDb db;

    public AddressBook(final AddressDb db) {
        this.db = db;
    }

    public boolean hasMobile(final String name) throws PersonNotFoundException, MultipleIdenticalRecordsException {
        Person person = null;
        try {
            person = db.findPerson(name);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        final PhoneNumber phoneNumber = person.getPhoneNumber();
        if (phoneNumber == null)
            return false;
        return phoneNumber.isMobile();
    }

    public int getSize() {
        try {
            return db.getSize();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

    }

    /**
     * Gets the given user's mobile phone number,
     * or null if he doesn't have one.
     */
    public String getMobile(final String name) throws MultipleIdenticalRecordsException {
        Person person = null;
        try {
            person = db.findPerson(name);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        final PhoneNumber phone = person.getPhoneNumber();
        if ((phone != null)&&(phone.isMobile())) {
            return phone.getNumber();
        }
        return null;
    }

    /**
     * Returns all names in the book truncated to the given length.
     */
    public List<String> getNames(int maxLength) {
        final List<Person> people;
        try {
            people = db.getAll();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        final List names = new LinkedList<String>();
        for (Person person : people) {
            final String name = person.getName();
            if (name.length() > maxLength) {
                names.add(name.substring(0, maxLength));
            } else {
                names.add(name);
            }
        }
        return names;
    }

    /**
     * Returns all people who have mobile phone numbers.
     */
    public List<Person> getPersonsHavingMobilePhone() {
        List<Person> people = null;
        try {
            people = db.getAll();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        final List list = new LinkedList();
        for (final Person person : people) {
            final PhoneNumber phoneNumber = person.getPhoneNumber();
            if ((phoneNumber != null) && (phoneNumber.isMobile())) {
                list.add(person);
            }
        }

        return list;
    }

}
