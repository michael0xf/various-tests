package addressbook.model;
/*
 * Sorry, javadocs empty.
 */
public class Person {
    private String name;
    private PhoneNumber phoneNumber;

    public Person(final String name, final PhoneNumber phoneNumber) {
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public PhoneNumber getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(final PhoneNumber phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

}
