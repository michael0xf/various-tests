import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: MKRAFT
 * Date: 1/21/15
 * Time: 1:03 AM
 * To change this template use File | Settings | File Templates.
 */

/*
   The implements of CacheMap please see in CacheMapImpl.java.
 */

/*
 * Sorry, javadocs empty.
 */

public class CacheMachine<KeyType,ValueType>{
    protected List<Long> timeQueue;
    protected List<KeyType> keyList;
    protected Map<KeyType,Integer> position;

    public CacheMachine(final int capacity){
        clear(capacity);
    }

    public void clear(final int capacity) {
        this.keyList = new ArrayList<KeyType>(capacity);
        this.timeQueue = new ArrayList<Long>(capacity);
        this.position = new HashMap<KeyType, Integer>(capacity);
    }

    public void put(final KeyType key) {
        int index = timeQueue.size();
        this.timeQueue.add(Clock.getTime());
        this.keyList.add(key);
        this.position.put(key, index);
    }

    public void remove(Object key){
        if (!this.position.containsKey(key))
            return;
        int index = this.position.remove(key);
        if (index >= 0){
            this.timeQueue.remove(index);
            this.keyList.remove(index);
        }
    }


    public void clearExpired(final Map<KeyType, ValueType> cache, final long timeToLive) {
        final long expireTime = Clock.getTime() - timeToLive;
        final int index = find(expireTime);
        for(int i = 0; i < index; i++){
            KeyType key = keyList.get(i);
            this.position.remove(key);
            cache.remove(key);
        }
        this.timeQueue = this.timeQueue.subList(index, this.timeQueue.size());
        this.keyList = this.keyList.subList(index, this.keyList.size());
    }

    protected int find(final long time) {
        final int size = this.timeQueue.size();
        int min = 0, count = size;
        while (count > 0) {
            count = count >> 1;
            final int middle = min + count;
            if (middle < size) {
                final long value = this.timeQueue.get(middle).longValue();
                if (time > value){
                    min = middle + 1;
                }else if (time == value){
                    return middle;
                }
            } else if (count == 0){
                return size;
            }
        }
        return min;
    }

    public void clear(){
        this.timeQueue.clear();
        this.keyList.clear();
        this.position.clear();

    }


}
