import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: MKRAFT
 * Date: 1/21/15
 * Time: 10:30 PM
 * To change this template use File | Settings | File Templates.
 */
/*
 * Sorry, javadocs empty.
 */

public class CacheMapImpl<KeyType, ValueType> extends HashMap<KeyType, ValueType> implements CacheMap<KeyType, ValueType>{

    private CacheMachine<KeyType, ValueType> cacheMachine;
    private long timeToLive = 0;



    public CacheMapImpl(final int capacity){
        super(capacity);
        this.cacheMachine = new CacheMachine<KeyType, ValueType>(capacity);
    }

    @Override
    public void setTimeToLive(final long timeToLive) {
        this.timeToLive = timeToLive;
    }

    @Override
    public long getTimeToLive() {
        return timeToLive;
    }


    @Override
    public ValueType put(final KeyType key, final ValueType value) {
        clearExpired();
        this.cacheMachine.put(key);
        return super.put(key, value);
    }

    @Override
    public void clearExpired() {
        this.cacheMachine.clearExpired(this, timeToLive);
    }

    @Override
    public void clear() {
        this.cacheMachine.clear();
        super.clear();
    }

    @Override
    public boolean containsKey(Object key) {
        clearExpired();
        return super.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        clearExpired();
        return super.containsValue(value);
    }

    @Override
    public ValueType get(Object key) {
        clearExpired();
        return super.get(key);
    }

    @Override
    public boolean isEmpty() {
        clearExpired();
        return super.isEmpty();
    }

    
    @Override
    public ValueType remove(Object key){
        cacheMachine.remove(key);
        return super.remove(key);
    }

    @Override
    public int size() {
        clearExpired();
        return super.size();
    }

}
