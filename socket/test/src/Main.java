
import com.sun.istack.internal.NotNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Map;

public class Main {

    public static void main(final String args[]) {
        final int portNumber = 8083;
        final Thread reader = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    System.out.println("read...");
                    System.out.println(read(portNumber));
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            }
        });
        reader.start();


        final Thread writer = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("write...");
                do {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    try {
                        System.out.println("write 1000...");
                        write(portNumber, 1000);
                        return;
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }while (true);
            }
        });
        writer.start();
    }

    public static int read(final int portNumber)throws IOException, NumberFormatException {
            ServerSocket serverSocket = new ServerSocket(portNumber);
            Socket clientSocket = serverSocket.accept();
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(clientSocket.getInputStream()));
            final String s = in.readLine();
            return Integer.parseInt(s.trim());

    }


    public static void write(final int portNumber, final inbt number) throws IOException {
        final String hostName = "localhost";
        final Socket echoSocket = new Socket(hostName, portNumber);
        final PrintWriter out =
                new PrintWriter(echoSocket.getOutputStream(), true);
        out.write(number + "\r\n");
        out.flush();
    }

}
